const express = require('express');
const session = require('express-session');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const User = require('./server/models/user');
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const user = require('./server/api/client/user')
const auth = require('./server/api/client/auth')
const course = require('./server/api/client/course')
const config = require('../config')

const app = express();

app.use(bodyParser.json());

//middleware
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(session({
    secret: 'secrettexthere',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());

//passport configuration
passport.use(new LocalStrategy(User.authenticate({
    passReqToCallback: true
})))
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


//router
app.use('/user', user);
app.use('/auth', auth);
app.use('/course', course);
app.use(express.static('app/static'));
app.use('/image', express.static(config.IMAGE_FOLDER))
app.use('/scripts', express.static(config.ROOT + '/node_modules/jquery-datetimepicker/build/'))

//database
mongoose.connect('mongodb://localhost:27017/skillmatic');


//start app
app.listen(3000, function() {
    console.log('application listening on port 3000');
});
