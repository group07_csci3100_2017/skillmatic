/*
 * createUser(username, password, email, selfIntroduction)
 *  create user
 * edit user info
 * validate password
 *
 */
var mongoose = require('mongoose');
var User = require('../../models/user');

module.exports = {
  getUsers: function(res) {
    User.find(function(err, users) {
      if (err) res.sent(err);
      res.json(users);
    });
  },

  createUser: function(username, email, selfIntroduction, res) {
    User.register({username: username, email: email, selfIntroduction: selfIntroduction}, function(err){
      if(err) res.send(err);
      res.json({'message':'User created successfully.'});
    })
  },

  changeUserPassword: function(userId, oldpassword, password, res){
    User.findById(userId, function(err, user){
      if (err) res.send(err);
      if (user.password == password) 
        user.save(function(err){
          if (err) res.send(err);
          res.json({'message': 'Password changed successfully.'});
        });
    });
  },

  changeUserSelfIntroduction: function(userId, new_selfIntro, res){
    User.findByIdAndUpdate(userId, { selfIntroduction: new_selfIntro }, function(err){
      if (err) res.send(err);
      res.json({'message': 'Self-introduction changed successfully.'});
    });
  },

  addCourseHeld: function(userId, courseId) {
    User.findByIdAndUpdate(userId, { $push: { coursesHeld: courseId } }, function(err) {
      if (err) res.send(err);
      res.json({'message': 'Course added to coursesHeld successfully.'});
    });
  },

  addEnrollment: function(userId, enrollmentId) {
    User.findByIdAndUpdate(userId, { $push: { enrollments: enrollmentId } }, function(err) {
      if (err) res.send(err);
      res.json({'message': 'Course added to coursesHeld successfully.'});
    });
  },

}
