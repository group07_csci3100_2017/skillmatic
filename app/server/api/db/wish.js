/*
 * create wish
 * vote wish
 * approve wish
 * get approvals
 * get voters
 */
var mongoose = require('mongoose');
var Wish = require('../../models/wish');

module.exports = {
  //function: createWish(proposerId, description, res) {
  getWishes: function(res) {
    Wish.find(function(err, wishes) {
      if (err) res.sent(err);
      res.json(wishes);
    });
  },

  createWish: function(description, res) {
    var wish = new Wish();
    //wish.proposer = proposerId;
    wish.description = description;
    wish.voters = [];
    wish.courses = [];
    wish.save(function(err){
      if (err) res.send(err);
      res.json({'message':'Wish created successfully'});
    });
  },

  voteWish: function(wishId, voterId, res) {
    Wish.findByIdAndUpdate(wishId, { $push: { voters: voterId } }, function(err) {
      if (err) res.send(err);
      res.json({'message': 'VoterId added to voters changed successfully'});
    });
  },

  addCourse: function(wishId, courseId, res) {
    Wish.findByIdAndUpdate(wishId, { $push: { courses: courseId } }, function(err) {
      if (err) res.send(err);
      res.json({'message': 'Course added to courses successfully'});
    });
  },
};
