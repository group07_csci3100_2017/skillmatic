/*
 * create course
 * edit course info
 * delete course
 *
 * create session
 * edit session info
 * delete session
 *
 * get session rating
 * get course rating
 *
 * search course
 * search session
 */

var mongoose = require('mongoose');
var courseModel = require('./course');

module.exports = {
  createCourse: function(instructor, title, description, duration, fee, maxClassSize, categories, res) {
    var course = new Course();
    course.instructor = instructor;
    course.title = title;
    course.description = description;
    course.duration = duration;
    course.fee = fee;
    course.maxClassSize = maxClassSize;
    course.categories = categories;

    course.save(function(err){
      if (err) res.send(err);
      res.json('message':'Course created successfully.');
    });
  },
}
