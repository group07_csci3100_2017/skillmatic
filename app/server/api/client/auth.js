/**
 * This file implment authentication api
 */

var express = require('express');
var passport = require('passport');
var User = require('../../models/user');
var router = express.Router();

/**
 * register api
 * @req.username: string
 * @req.email: string
 * @req.password: string
 * @req.selfInfroduction: string
 */
router.post('/register', function(req, res) {
    var username = req.body.username;
    var email = req.body.email;
    var selfIntroduction = req.body.selfIntroduction;
    var password = req.body.password;
    User.register({ username: username, email: email, selfIntroduction: selfIntroduction }, password, function(err) {
        if (err) return res.send(err);
        res.json({ 'message': 'User created successfully.' });
    });
});

/**
 * user's login status
 * return login if user login, otherwise notlogin
 */
router.get('/status', function(req, res) {
    if (!req.isAuthenticated()) return res.json({ message: 'notlogin' });
    res.json({ message: 'login' });
});

/**
 * login api, use passport.js to implement
 * @req.username: string
 * @req.password: string
 */
router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) {
            return res.send('User does not exist')
        }
        req.logIn(user, function(err) {
            if (err) { return next(err); }
            return res.json({ username: user.username, user: user });
        });
    })(req, res, next);
});

/**
 * logout
 * return logout message after successfully logout
 */
router.get('/logout', function(req, res) {
    req.logout();
    res.json({ message: 'logout' });
});

module.exports = router;
