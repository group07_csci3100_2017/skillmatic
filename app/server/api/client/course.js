/**
 * This file implement course related api
 */

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../../models/user');
var Course = require('../../models/course');
var Session = require('../../models/session');
var Enrollment = require('../../models/enrollment');
var bodyParser = require('body-parser');

/**
 * {post} /course Return particular course if specified, or list of all courses, with populated sessions
 * @req.param {ObjectId} courseId (optional)
 * if provided courseId * single json
 *   @res {json} Document of the course
 *   @res {json[]} sessions Documents of sessions of the course that are still open
 * else                 * list of json
 *   @res {json[]} courses Document of all courses
 *     courses {json[]} sessions Documents of sessions of each course that are still open
 */
router.post('/', function(req, res) {
    var query = (req.body.courseId) ? Course.findById(req.body.courseId) : Course.find();
    query.populate({
        path: 'sessions',
        select: 'venue openDateTime classDateTime enrollments lat lng',
        match: { openDateTime: { $gte: Date.now() } },
        options: { sort: { classDateTime: -1 } }
    }).exec(function(err, courses) {
        if (err) res.send(err);
        res.json(courses);
    });
});

router.get('/', function(req, res) {
    //render search page
});

/**
 * {post} /course/search Return list of courses satisfying search criteria
 * @req.body {String} keyword Substrings of title or description of course (optional)
 * @req.body {Number} durationMax Upper limit of duration of course (optional)
 * @req.body {Number} durationMin Lower limit of duration of course (optional)
 * @req.body {Number} feeMax Upper limit of fee of course (optional)
 * @req.body {Number} feeMin Lower limit of fee of course (optional)
 * @req.body {[String]} category strings where at least one of them is in course's categories (optional)
 * @req.body {String} instructorUN Username of instructor of course (optional)
 * @res {json[]} courses Documents of courses satisfying search criteria
 *   courses {json[]} sessions Documents of sessions of the course that are still open
 *   courses {json[]} instructor Documents of instructor (only username)
 */
router.post('/search', function(req, res) {
    var keyword = (req.body.keyword) ? req.body.keyword : "";
    var durationMax = (req.body.durationMax) ? req.body.durationMax : 1440;
    var durationMin = (req.body.durationMin) ? req.body.durationMin : 0;
    var feeMax = (req.body.feeMax) ? req.body.feeMax : 999999;
    var feeMin = (req.body.feeMin) ? req.body.feeMin : 0;
    var categoriesAny = (req.body.categoriesAny) ? req.body.categoriesAny : ['Sports', 'DIY', 'Music', 'Cooking', 'Arts', 'Others'];

    Course.find({

        $or: [{ 'title': { '$regex': keyword, '$options': 'i' } }, { 'description': { '$regex': keyword, '$options': 'i' } }],
        duration: { $gte: durationMin, $lte: durationMax },
        fee: { $gte: feeMin, $lte: feeMax },
        categories: { $elemMatch: { $in: categoriesAny } }
    }).limit(8).populate({
        path: 'instructor',
        select: 'username',
    }).populate({
        path: 'sessions',
        match: { openDateTime: { $gte: Date.now() } },
        select: 'venue classDateTime lat lng'
    }).exec(function(err, courses) {
        if (err) res.send(err);
        if (req.body.instructorUN) {
            courses = courses.filter(function(course) {
                return (course.instructor.username == req.body.instructorUN);
            });
        }
        res.json(courses);
    });
})

/**
 * {get} /course/enroll?sessionId={SESSION_ID}  Provide form to enroll a course, if logged in
 * @req.query {ObjectId} sessionId Id of the enrolling session
 * @res {json} session Document of session
 *  session {json} coursePlan Document of the course, only title, profile_image, and fee
 */
router.get('/enroll', function(req, res) {
    //render session enrollment page
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    if (!req.query.sessionId) res.send('sessionId required');
    Session.find({ _id: req.query.sessionId, openDateTime: { $gte: Date.now() } }).populate({
        path: 'coursePlan',
        select: 'title profile_image fee'
    }).exec(function(err, session) {
        if (err) res.send(err);
        if (!session) res.send('session not found; maybe no matching sessionId, or has passed opening date time');
        res.json(session);
    });
});

/**
 * {post} /course/enroll?sessionId={SESSION_ID}  Enroll a course, if logged in and session still open
 * @req.query {ObjectId} sessionId Id of the session to be enrolled
 */
router.post('/enroll', function(req, res) {
    if (!req.isAuthenticated()) return res.redirect('/auth/login');
    if (!req.query.sessionId) return res.send('sessionId require');
    Session.findOne({ _id: req.query.sessionId, openDateTime: { $gte: Date.now() } }).populate({
        path: 'coursePlan', 
        select: 'maxClassSize instructor'
    }).exec(function(err, session) {
        if (err) return res.send(err);
        else if (!session) return res.send('session not found; maybe no matching sessionId, or has passed opening date time');
        else if (String(session.coursePlan.instructor) == String(req.user._id)) return res.send({ 'message': 'cannot enroll course created by yourself'});
        else if (session.coursePlan.maxClassSize > 0 && session.enrollments.length >= session.coursePlan.maxClassSize) return res.send({ 'message': 'session fulled' });
        Enrollment.findOne({ session: req.query.sessionId, student: req.user._id }).exec(function(err1, prevEnrollment) {
            if (prevEnrollment) return res.json({'message': 'already enrolled to session'});
            var enrollment = new Enrollment();
            enrollment.student = req.user._id;
            enrollment.session = session._id;
            enrollment.course = session.coursePlan;
            enrollment.commented = false;
            enrollment.save(function(err2) {
                if (err2) return res.send(err2);
                User.findByIdAndUpdate(enrollment.student, { $push: { "enrollments": enrollment._id } }, function(err3, user) {
                    if (err3) return res.send(err3);
                    user.enrollments.push(enrollment._id);
                    user.save(function(err4) {
                        if (err4) return res.send(err4);
                        return res.json({ 'message': 'enrolled to session successfully' });
                    });
                });
            });
        });
    });
});

/**
 * {get} /course/comment?enrollmentId={USER_ID}  Provide form to comment on a course, if logged in and enrolled
 * @req.query {ObjectId} enrollmentId Id of the enrollment
 * @res {json} enrollment Document of enrollment
 *  enrollment {json} course Document of the course, only title, profile_image, and fee
 *  enrollment {json} session Document of the session, only classDateTime and venue
 */
router.get('/comment', function(req, res) {
    //render comment page
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    if (!req.query.enrollmentId) res.send('enrollmentId required');
    Enrollment.find({ _id: req.query.enrollmentId, student: req.user._id, commented: false }).populate({
        path: 'course',
        select: 'title profile_image fee'
    }).populate({
        path: 'session',
        match: { classDateTime: { $gte: Date.now() } },
        select: 'classDateTime venue'
    }).exec(function(err, enrollment) {
        if (err) res.send(err);
        if (!enrollment) res.send('enrollment not found; maybe no matching enrollmentId, mis-matched studentId, or has been commented');
        if (!enrollment.session) res.send('session not yet held');
        res.json(enrollment);
    });
});

/**
 * {post} /course/comment?enrollmentId={USER_ID}  Comment on a course, if logged in and enrolled to a session of the course
 * @req.query {objectId} enrollmentId
 * @req.body {Number} rating Rating of course; from 1 to 5, inclusive
 * @req.body {String} text Comment text of course (optional)
 */
router.post('/comment', function(req, res) {
    if (!req.isAuthenticated()) return res.redirect('/auth/login');
    if (!req.query.enrollmentId) return res.send('enrollmentId required');
    if (!req.body.rating) return res.send('rating required');
    Enrollment.findById(req.query.enrollmentId).where({student: req.user._id }).populate('course', '_id').populate({
        path: 'session',
        match: { classDateTime: { $gte: new Date(1970) } },
        select: 'classDateTime venue'
    }).exec(function(err, enrollment) {
        if (err) return res.send(err);
        if (!enrollment) return res.send('enrollment not found');
        if (!enrollment.session) return res.send('session not yet held');
        if (enrollment.commented) return res.send('already commented');
        else {
            var comment = {};
            comment.studentId = req.user._id;
            comment.studentUsername = req.user.username;
            comment.commentDateTime = Date.now();
            comment.sessionDateTime = enrollment.session.classDateTime;
            comment.sessionVenue = enrollment.session.venue;
            comment.rating = req.body.rating;
            comment.text = (req.body.text) ? req.body.text : '';
            Course.findByIdAndUpdate(enrollment.course._id, { $push: { 'comments': comment } }, function(err2, course) {
                if (err2) return res.send(err2);
                enrollment.commented = true;
                enrollment.save(function(err3) {
                    if (err3) return res.send(err3);
                    else return res.send('comment saved successfully');

                });
            });
        }
    });
});

/**
 * get the seeions the user enrolled
 * return a list of sessions current user enrolled
 */
router.get('/session', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    if (!req.query.sessionId) res.send('sessionId required');
    Session.findById(req.query.sessionId, 'title classDateTime duration').exec(function(err, session) {
        if (err) {
            res.send(err);
        } else {
            res.send(session);
        }
    });
})

module.exports = router;
