/**
 * This file implement user related api
 */

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../../models/user');
var Course = require('../../models/course');
var Session = require('../../models/session');
var bodyParser = require('body-parser');
var Enrollment = require('../../models/enrollment');
var fs = require('fs')
var formidable = require('formidable');
var crypto = require('crypto');
var passport = require('passport');
const config = require('../../../../config')

/**
 * {get} /user/profile Return user's profile, if logged in
 * @res {json} Document of the User
 */
router.get('/profile', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    res.json(req.user);
});


/**
 * {post} /user/profile Update user's profile, if logged in
 * req only contains fields to be updated
 * @req.body {String} email New email (optional)
 * @req.body {String} selfIntroduction New self introduction (optional)
 * @req.body {String} newPassword New password (optional)
 * @req.body {String} oldPassword Old password (required if updating newPassword)
 */
router.post('/profile', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    User.findById(req.user._id, function(err, user) {
        if (err) return res.send(err);
        if (req.body.email) user.email = req.body.email;
        if (req.body.selfIntroduction) user.selfIntroduction = req.body.selfIntroduction;
        user.save(function(err2) {
            if (err2) return res.send(err2);
            if (req.body.newPassword) {
                if (!req.body.oldPassword) return res.send('original password require');
                user.authenticate(req.body.oldPassword, function(err3, userAuth, info) {
                    if (err3) return res.send(err3);
                    if (!userAuth) return res.send('incorrect original password; info except password changed');
                    else user.setPassword(req.body.newPassword, function(err4) {
                        if (err4) return res.send(err4);
                        return res.send('account info changed successfully');
                    });
                });
            } else return res.send('account information changed successfully');
        });
    });
});

/**
 * {get} /user?userId={USER_ID} return information by USER_ID
 */
router.get('/', function(req, res) {
    if (!req.query.userId) return res.send('userId required');
    User.findById(req.query.userId, 'username email enrollments coursesHeld').populate({
        path: 'enrollments',
        select: 'course',
        populate: { path: 'course', select: 'title description profile_image' }
    }).populate({
        path: 'coursesHeld',
        select: 'title description profile_image'
    }).exec(function(err, user) {
        if (err) return res.send(err);
        res.json(user);
    })
})

/** 
 * {get} /user/enrollments Return sessions/courses that user enrolled, if logged in
 * @res {json[]} sessions Documents of sessions enrolled by the user;
 *   sessions {json} coursePlan Document of Course referenced by each session
 */
router.get('/enrollments', function(req, res) {
    if (!req.isAuthenticated()) return res.redirect('/auth/login');
    Enrollment.find({_id: {$in: req.user.enrollments}}).populate({path: "session"}).exec(function(err, enrolls){
        var sess = [];
        for(var enroll of enrolls){
            sess.push({session: enroll.session, enrollmentId: enroll._id});
        }
        return res.json(sess);
    })
});

/**
 * {get} /user/courses Return courses that user created, if logged in
 * @res {json[]} courses Documents of courses created by the user;
 *   courses {json[]} sessions list of Document of Sessions belong to each course
 */
router.get('/courses', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    Course.find({ _id: { $in: req.user.coursesHeld } }).populate('sessions').exec(function(err, courses) {
        res.json(courses);
    });
});

/**
 * {post} /user/courses Create new course, if logged in
 * @req.body {String} title Title of the course
 * @req.body {String} description Description of the course
 * @req.body {Number} duration Time each session lasts, in minutes
 * @req.body {Number} fee Fee for user to enroll a session
 * @req.body {Number} maxClassSize Maximum number of users could enroll to a class (optional)
 * #req.body {String[]} categories List of categories the course belongs to; empty array if none
 */
router.post('/courses', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if (err) {
            return res.send(err);
        }
        var course = new Course();
        course.instructor = req.user._id;
        course.title = fields.title;
        course.description = fields.description;
        course.duration = fields.duration;
        course.fee = fields.fee;
        course.maxClassSize = (fields.maxClassSize) ? fields.maxClassSize : 0;
        course.categories = fields.categories.split(',');
        course.profile_image = config.IMAGE_FOLDER + crypto.randomBytes(20).toString('hex') + '.jpg'
        if (files.profile_image) {
            fs.rename(files.profile_image.path, course.profile_image, function(err) {
                if (err) return res.send(err);
            });
        } else {
            fs.rename(config.PLACEHOLDER, course.profile_image, function(err) {
                if (err) return res.send(err);
            });
        }
        course.save(function(err) {
            if (err) return res.send(err);
            User.findByIdAndUpdate(course.instructor, { $push: { "coursesHeld": course._id } }, function(err2, user) {
                if (err2) return res.send(err);
                else return res.send('course saved');
            });
        });
    });
});

/**
 * {put} /user/courses Update info of course created by the user, if logged in
 * @req.body {ObjectId} courseId Id of the course to be updated
 * @req.body Fields if to be updated, format refer to post (optional)
 */
router.put('/courses', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if (err) {
            res.send(err);
        }
        Course.findById(fields.courseId, function(err, course) {
            if (err) res.send(err);
            // validate current user is instructor of the course
            if (String(course.instructor) != String(req.user._id)) res.send({ 'message': 'Permission denied: only instructor of the course can edit course' });
            if (fields.title) course.title = fields.title;
            if (fields.description) course.description = fields.description;
            if (fields.duration) course.duration = fields.duration;
            if (fields.fee) course.fee = fields.fee;
            if (fields.maxClassSize) course.maxClassSize = fields.maxClassSize;
            if (fields.categories) course.categories = fields.categories;
            course.save(function(err2) {
                if (err2) res.send(err2);
                if (files.profile_image) {
                    fs.rename(files.profile_image.path, course.profile_image, function(err) {
                        if (err) res.send(err);
                    });
                }
                res.json(course);
            });
        });
    });

});

/**
 * {post} /user/course/session Create new sesion for course, if logged in and matches instructorId of the course
 * @req.body {ObjectId} courseId Id of the referencing course
 * @req.body {String} remark Description on the session addition to course
 * @req.body {String} venue Address of the session to be held
 * @req.body {String} classDateTime Date and time of the session to be held, in format "MM/DD/YYYY HH:mm"
 * @req.body {String} openDateTime Session is open for registration until that date and time, in format "MM/DD/YYYY HH:mm"
 */
router.post('/course/session', function(req, res) {
    if (!req.isAuthenticated()) return res.redirect('/auth/login');
    Course.findById(req.body.courseId, function(err, course) {
        if (err) return res.send(err);
        // validate current user is instructor of the course
        if (String(course.instructor) != String(req.user._id)) return res.send({ 'message': 'Permission denied: only instructor of the course can create session' });
        var session = new Session();
        session.coursePlan = course._id;
        session.remark = req.body.remark;
        session.venue = req.body.venue;
        session.classDateTime = new Date(req.body.classDateTime);
        session.openDateTime = new Date(req.body.openDateTime);
        session.enrollments = [];
        session.duration = course.duration;
        session.title = course.title;
        session.lat = req.body.lat;
        session.lng = req.body.lng;
        session.save(function(err2) {
            if (err2) return res.send(err2);
            course.sessions.push(session);
            course.save(function(err3) {
                if (err) return res.send(err3);
                return res.json({session});
            });
        });
    });
});

/**
 * {put} /user/course/session Update info of session, if logged in and matches instructorId of the course
 * @req.body {ObjectId} sessionId Id of the referencing course
 * @req.body Fields if to be updated, format refer to post (optional)
 */
router.put('/course/session', function(req, res) {
    if (!req.isAuthenticated()) res.redirect('/auth/login');
    Session.findById(req.body.sessionId).populate('coursePlan', 'instructor').exec(function(err, session) {
        if (err) res.send(err);
        // validate current user is instructor of the course
        if (String(session.coursePlan.instructor) != String(req.user._id)) res.send({ 'message': 'Permission denied: only instructor of the course can edit session' });
        if (req.body.remark) session.remark = req.body.remark;
        if (req.body.venue) session.venue = req.body.venue;
        if (req.body.classDateTime) session.classDateTime = new Date(req.body.classDateTime);
        if (req.body.openDateTime) session.openDateTime = new Date(req.body.openDateTime);
        session.save(function(err2) {
            if (err2) res.send(err2);
            res.json({ 'message': 'session updated successfully' });
        });
    });
});

module.exports = router;
