/**
 * This file declare the wish model for database
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var user = require('./user');
var course = require('./course');

var wishSchema = mongoose.Schema({
  proposer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  description: String,
  voters: [ { type: mongoose.Schema.Types.ObjectId, ref: 'User' } ],
  success: { type: Boolean, default: false },
  courses: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Course' } ]
});

wishSchema.plugin(timestamps);

module.exports = mongoose.model('Wish', wishSchema);
