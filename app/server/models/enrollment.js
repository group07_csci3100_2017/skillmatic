/**
 * This file declare the enrollment model for database
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var user = require('./user');
var enrollment = require('./enrollment');

var enrollmentSchema = mongoose.Schema({
  student: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  session: { type: mongoose.Schema.Types.ObjectId, ref: 'Session' },
  course: { type: mongoose.Schema.Types.ObjectId, ref: 'Course' },
  commented: Boolean
});

enrollmentSchema.plugin(timestamps);

module.exports = mongoose.model('Enrollment', enrollmentSchema);
