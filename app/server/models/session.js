/**
 * This file declare the session model for database
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var course = require('./course');
var enrollment = require('./enrollment');

var sessionSchema = mongoose.Schema({
  coursePlan: { type: mongoose.Schema.Types.ObjectId, ref: 'Course'},
  remark: String,
  venue: String,
  classDateTime: Date,
  openDateTime: Date,
  duration: Number,
  title: String,
  lat: Number,
  lng: Number,
  enrollments: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Enrollment' } ]
});

sessionSchema.plugin(timestamps);

module.exports = mongoose.model('Session', sessionSchema);
