/**
 * This file declare the course model for database
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var user = require('./user');
var session = require('./session');


var courseSchema = mongoose.Schema({
  instructor: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  title: String,
  description: String,
  profile_image: String,
  duration: Number,
  fee: Number,
  maxClassSize: { type: Number, default: 0 },
  sessions: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Session' } ],
  categories: [String],
  comments: [ {
    studentId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    studentUsername: String,
    commentDateTime: Date,
    sessionDateTime: Date,
    sessionVenue: String,
    rating: Number,
    text: String
  } ]
});

courseSchema.plugin(timestamps);
module.exports = mongoose.model('Course', courseSchema);
