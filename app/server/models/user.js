/**
 * This file declare the user model for database
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var passportLocalMongoose = require('passport-local-mongoose');
var course = require('./course');
var enrollment = require('./enrollment');
var wish = require('./wish');

var userSchema = mongoose.Schema({
  username: { type: String, unique: true },
  email: { type: String, unique: true },
  selfIntroduction: String,
  coursesHeld: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Course'}],
  enrollments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Enrollment'}],
  wishesVoted: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Wish'}]
});

userSchema.plugin(timestamps);
userSchema.plugin(passportLocalMongoose);

userSchema.methods.validatePassword = function validatePassword(inputPassword) {
  return this.password == inputPassword;
}

module.exports = mongoose.model('User', userSchema);
