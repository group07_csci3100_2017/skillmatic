/**
 * This file declare the message model for database
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var user = require('./user');
var chatgroup = require('./chatgroup');

var messageSchema = mongoose.Schema({
  content: String,
  sender: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  toChatgroup: { type: mongoose.Schema.Types.ObjectId, ref: 'Chatgroup'}
});

messageSchema.plugin(timestamps);

module.exports = mongoose.model('Message', messageSchema);
