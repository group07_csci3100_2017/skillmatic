/**
 * This file declare the chatgroup model for database
 */

var mongoose = require('mongoose');
var user = require('./user');
var message = require('./message');

var chatgroupSchema = mongoose.Schema({
  members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message'}]
});

module.exports = mongoose.model('Chatgroup', chatgroupSchema);
