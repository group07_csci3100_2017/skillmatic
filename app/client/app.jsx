var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');
var Main = require('Main');
var Content = require('Content');
var About = require('About');
var Course = require('Course');
var Register = require('Register');
var User_course_class = require('User_course_class');
//this will import Main, Content, About, Course, Register nad User_course_class modules.
//by using these module we can allow the react router to create the path detector.
ReactDOM.render(
    <Router history = {hashHistory}>
        <Route path="/" component = {Main}>
            <IndexRoute component = {Content}/>
            <Route path = "about" component = {About}/>
            <Route path = "course" component = {Course}/>
            <Route path = "teacher" component = {User_course_class} />
        </Route>
    </Router>
    ,document.getElementById('app')
);
