// this contains all the api calls from the server
var axios = require('axios');
const COURSE_API_URL = '/course/search';
const USER_API_URL = '/user';
const USER_COURSE_API_URL = '/user/courses'
const USER_SESSION_API_URL = '/user/course/session'
const SESSION_API_URL = '/user/enrollments'
const COMMENT_API_URL = 'course/comment'
const ENROLL_URL = '/course/enroll'
const data_config = { headers: { 'Content-Type': 'multipart/form-data' } };
module.exports = {
    /**
     * {post} /course/search this will search the course by the parameter category;
     */
    getCoursesbyCat: function(category){
        var encodedcategory = encodeURIComponent(category);
        var requestUrl = `${COURSE_API_URL}`;
        return axios.post(requestUrl,{categoriesAny:category}).then(function (res){
                if(res.data.cod && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res.data;
                }
        }, function (res) {
                throw new Error(res.data.message);
        });
    },
    /**
     * {post} /course/search this will search the course by related parameters;
     */
    getCoursesby: function(category,keyword,durationMax,durationMin,feeMax,feeMin){
        var forinputcate = category;
        if(category.length === 0) forinputcate = null;
        var encodedkeyword = encodeURIComponent(keyword);
        var encodeddurationMax = encodeURIComponent(durationMax);
        var encodeddurationMin = encodeURIComponent(durationMin);
        var encodedFeeMax = encodeURIComponent(feeMax);
        var encodedFeeMin = encodeURIComponent(feeMin)
        var requestUrl = `${COURSE_API_URL}`;
        return axios.post(requestUrl,{categoriesAny:forinputcate, keyword:keyword, durationMax: durationMax, durationMin: durationMin, feeMax: feeMax, feeMin:feeMin}).then(function (res){
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res.data;
                }
        }, function (res) {
                throw new Error(res.data.message);
        });
    },
     /**     
     * {post} /course/search this will search the course by Id;
     */
    getCourseById: function(courseId) {
        var data = {courseId: courseId};
        var requestUrl = '/course';
        return axios.post(requestUrl, data).then(function(res) {
                if(res.data.code && res.data.message) {
                        throw new Error(res.data.message);
                }else {
                        return res.data;
                }
        }, function(res) {
                throw new Error(res.data.message);
        });
    },
    /**     
     * {get} /user this will get the user by Id;
     */
    getInstructor: function(instructorID){
        var requestUrl = `${USER_API_URL}`;
        return axios.get(requestUrl+'/?userId='+instructorID).then(function(res){
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res.data;
                }
        },function(res){
                throw new Error(res.data);
        });
    },
    /**     
     * {get} /user/courses this will get courses created by user;
     */
    getUsersCourse: function(){
        var requestUrl = `${USER_COURSE_API_URL}`;
        return axios.get(requestUrl).then(function(res){
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res.data;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    },
     /**     
     * {post} /user/courses this will post to the server to create a new course;
     */
    createNewCourse: function(data){
        var requestUrl = `${USER_COURSE_API_URL}`;
        return axios.post(requestUrl,data,data_config).then(function(res){
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    },
     /**     
     * {put} /user/courses this will update the users course;
     */
    editCourse: function(data) {
        var requestUrl = `${USER_COURSE_API_URL}`;
        return axios.put(requestUrl,data,data_config).then(function(res){
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    },
    /**     
     * {post} /user/course/session this will let user to create session by the parameter data;
     */
    createSession: function(data){
        var requestUrl = `${USER_SESSION_API_URL}`;
        return axios.post(requestUrl, data).then(function(res) {
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res.data;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    },
     /**     
     * {put} /user/course/session this will let user to edit session by the parameter data;
     */
    editSession: function(data){
        var requestUrl = `${USER_SESSION_API_URL}`;
        return axios.put(requestUrl, data).then(function(res) {
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    },
    /**     
     * {get} /user/course/session this will let user to get thier enrolled session;
     */
    getEnrolledSession: function(){
        var requestUrl = `${SESSION_API_URL}`;
        return axios.get(requestUrl).then(function(res) {
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res.data;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    },
    /**     
     * {post} /course/enroll this will let user to enroll session by parameter essionId ;
     */
    enrollSession: function(session_id) {
        var requestUrl = `${ENROLL_URL}?sessionId=${session_id}`;
        return axios.post(requestUrl).then(function(res) {
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res;
                }
        },function(res){
                console.log(res);
        });
    },
     /**     
     * {post} /course/comment this will let user to leave comment by parameter enrollmentId;
     */
    postComment: function(enrollment_id, data) {
        var requestUrl = `${COMMENT_API_URL}?enrollmentId=${enrollment_id}`;
        return axios.post(requestUrl, data).then(function(res){
                if(res.data.code && res.data.message){
                        throw new Error(res.data.message);
                }else{
                        return res;
                }
        },function(res){
                throw new Error(res.data.message);
        });
    }
    
}
