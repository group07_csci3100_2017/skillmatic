var axios = require('axios');
const LOGIN_API_URL = '/auth/login';
const USER_API_URL = '/user/profile';
const STATUS_URL = '/auth/status';
const LOGOUT_API_URL = '/auth/logout';
const REGISTER_API_URL = '/auth/register'
module.exports = {
    /**     
     * {post} /auth/login this will let user to login;
     */
    loginfunction: function(username, password){
        var requestUrl = `${LOGIN_API_URL}`;
        return axios.post(requestUrl, {
            username: username,
            password: password
        },{auth:{
          username:username,
          password:password
        }})
      .then(function (response) {
        return response
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    /**     
     * {post} /user/profile by sending the selfIntroduction, email, newPassword, oldPassword;
     */
    changeprof: function(selfIntroduction,email,newpassword,oldpassword){
      var requestUrl = `${USER_API_URL}`;
      return axios.post(requestUrl,{
        selfIntroduction: selfIntroduction,
        email: email,
        newPassword: newpassword,
        oldPassword: oldpassword
      }).then(function(response){
        return response.data
      }).catch(function(err){
        console.log(err);
      });
    },
    /**     
     * {get} /auth/status this allow the frontend to check whether there is login user;
     */
    getStatus:function(){
      var requestUrl = `${STATUS_URL}`;
      return axios.get(requestUrl).then(function(res){
        return res.data.message;
      }).catch(function (err){
        console.log(err);
      });
    },
    /**     
     * {get} /auth/status this allow the user to logout;
     */
    logout: function(){
      var requestUrl = `${LOGOUT_API_URL}`;
      return axios.get(requestUrl).then(function(res){
        return res.data.message;
      }).catch(function (err){
        console.log(err)
      });
    },
    /**     
     * {get} /auth/status this allow the user to register;
     */
    register: function(username, password, email, selfIntroduction){
      var requestUrl = `${REGISTER_API_URL}`;
      return axios.post(requestUrl,{
        username:username,
        password:password,
        email:email,
        selfIntroduction:selfIntroduction
      }).then(function(res){
        return res;
      }).catch(function(err){
        console.log(err);
      });
    }
}
