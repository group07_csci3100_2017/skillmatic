var React = require('react');
var Comments = require('Comments');
var Comment_form = require('Comment_form');
var Course_information = require('Course_information');
var Course_cards = React.createClass({
    getDefaultProps: function(){
      return {
        cardNum:10,
      }  
    },
    /**
    * getInitialState() which will set the cardNum , cardsInfo to the props value
    * @cardNum t
    */
    getInitialState:function(){
        return {
            cardNum:this.props.cardNum,
            cardsInfo:this.props.cardsInfo,
        }
    },
    /**
    * renderBadge() after the Title inputbox being changed we'll the formData store in the state.
    * @category this will get the category and render the badges.
    */
    renderBadge:function(category){
        var forarray = [];
        category.map(function(cat){
            forarray.push(<span className="badge badge-info badge-pill" style={{marginLeft: '5px' }}>{cat}</span>)
        })
        return (<div> {forarray}</div>)
    },
    /**
    * renderImgModal() after the imgsrc and imgid being recieved we will render the imgModal
    */
    renderImgModal: function(imgsrc,id){
          return (<a role="button" data-toggle="modal" data-target = {'#'+id}>
                <img  className="card-img-top img-fluid" src={imgsrc} alt="Card image cap"  />
                </a>);
    },
    /**
    * renderCards() after the iscards is recieved we render the cards to the frontend.
    */
    renderCards:function(iscards){
        var therenderCards = [];
        var that = this;
        if(this.props.cardsInfo){
        this.props.cardsInfo.map(function(cards){
        var time = new Date(cards.createdAt);
            
            therenderCards.push(
                <div className="col-lg-3 " key={cards._id}>
                <div className="card divparagraph" >
                    <a role="button" data-toggle="modal" data-target={'#'+cards._id + (that.props.isIns? "ins" : "")}><img className="card-img-top img-fluid" src={"image/"+cards.profile_image.split('/')[2]} alt="Card image cap" /></a>
                    <div className="card-block">
                        <h4 className="card-title">{cards.title}</h4>
                        <p>{that.renderBadge(cards.categories)}</p>
                        <p className="card-text"><small className="text-muted">Create at {time.toLocaleString()}</small></p>
                        
                    </div>
                </div>
                </div>
                )
            }
        )
    }
    if (this.props.cardsInfo){
            this.props.cardsInfo.map(function(cards){
                therenderCards.push(
                    <Course_information rerender = {that.props.rerender} cards = {cards} isEnroll='not' isIns={that.props.isIns}/>
                )
            })
        }
    
        return therenderCards;
    },
    /**
    * render() this will render the frontend module
    */
    render:function(){
        return(
            <div className = "container">
                <div className="row">
                {this.renderCards()}        
                </div>
            </div>
           
        );
    }
}   
);
module.exports = Course_cards;
    
