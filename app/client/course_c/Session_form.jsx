var React = require('react');
var Venue_map = require('Venue_map');
var Course_api = require('Course_api');
var Session_form = React.createClass({
    /**
    * getInitialState() set the formData to empty.
    */
    getInitialState: function() {
        return {formData: {}};
    },
    /**
    * onPlacesChange() if the places change, we set the formData.lat and formData.lng to corresponding data.
    * @formData.lat is the latitude of the data
    * @formData.lng is the langitute of the data
    */
    onPlacesChanged: function(center) {
        var formData = this.state.formData;
        formData.lat = center.lat();
        formData.lng = center.lng();
        this.setState({formData: formData});
    },
    /**
    * onDateChange() if the date change, we set the formData.classDateTime and formData.openDateTime to corresponding data.
    * @formData.classDateTime is the classDateTime of the data
    * @formData.openDateTime is the openDateTime of the data
    */
    onDateChange: function(e) {
        console.log(e.target.value);
        var formData = this.state.formData;
        formData.classDateTime = e.target.value;
        formData.openDateTime = e.target.value;
        this.setState({formData: formData});
    },
    /**
    * onVenueChange() if the Venue change, we set the formData.venue to corresponding venue.
    * @formData.venue is the venue of the data
    */
    onVenueChange: function(e) {
        var formData = this.state.formData;
        formData.venue = e.target.value;
        this.setState({formData: formData});
    },
    /**
    * onSubmit() if the submit button pressed, we send the form to the Course_api.createSession
    * @formData sent by this submit
    */
    onSubmit: function(e){
        console.log(e);
        console.log(e.keyCode);
        console.log(e.target);
        e.preventDefault();
        if(e.keyCode == 13) return 
        this.state.formData.courseId = this.props.courseId;
        Course_api.createSession(this.state.formData).then(function(res) {
            
             swal({
                    title: "Successfully Create A Session",
                    text: "New session is created",
                    type: "success",
                    confirmButtonText: "OK"
                });
        }, function(err){
            console.log(err);
        })
    },
    /**
    * handleEnter() handle the enter button.
    */
    handleEnter: function(e) {
        if (e.key == 'Enter'){
            e.preventDefault();
            return false;
        }
        return true;
    },
    /**
    * render() render the module to the screen.
    */
    render: function() {
        return (<form onSubmit={this.onSubmit} onKeyPress={this.handleEnter} style={{width: "100%"}}>
            <div className="form-group" onKeyPress={this.handleEnter}>
                <strong htmlFor="date">Date</strong>
                {/*<input type="datetime-local" className="form-control"  onChange={this.onDateChange}/>*/}
                <input className="datetimepicker form-control" onChange={this.onDateChange} type="text" />
                <script>
                    {this.addDtpicker()}
                </script>
            </div>
            <div className="form-group" onKeyPress={this.handleEnter}>
                <strong htmlFor="venue">Venue</strong>
                <input type="text" className="form-control"   placeholder="Enter Venue" onChange={this.onVenueChange}/>
            </div>
            <div className="form-group">
                <Venue_map onPlacesChanged={this.onPlacesChanged} isIns={true}/>
            </div>
            <button type="submit" onKeyPress={this.handleEnter} className="btn btn-primary">Create Session</button>
        </form>);
    }
});

module.exports = Session_form;