var React = require('react');
var Course_api = require('Course_api');
var cookie = require('react-cookie');
import Rater from 'react-rater';

var Comment_form = React.createClass({
    /**
    * getInitialState() set the formData to empty.
    */
    getInitialState: function(){
        return {formData: {}}
    },
    /**
    * componentDidMount() set the formData to empty.
    */
    componentDidMount: function() {
        var tmp = this.state.formData;
        tmp.enrollmentId = this.props.enrollmentId;
        tmp.username = cookie.load('username');
        tmp.rating = 0;
        tmp.text = "";
        this.setState({username: cookie.load('username')});
    },
    /**
    * onTextChange() after the Text is changed, we upload the Text to formData.
    * @tmp.text is the e.target.value
    */
    onTextChange: function(e) {
        var tmp = this.state.formData;
        tmp.text = e.target.value;
    },
    /**
    * onRate() after the Rate is changed, we upload the to formData.
    * @tmp.rating is the e.rating
    */
    onRate: function(e) {
        var tmp = this.state.formData;
        tmp.rating = e.rating
    },
    /**
    * postComment() after the post button being pushed, we will upload the comment to the server
    * by calling the course_apo
    * @tmp.rating is the e.rating
    */
    postComment: function(e) {
        e.preventDefault();
        var that = this;
        var formData = this.state.formData;
        Course_api.postComment(formData.enrollmentId, formData).then(function(res){
            console.log(res);
            if(res.data !== 'rating required'){
            that.props.refresh();
            swal({
                    title: "Successfully Post A Comment",
                    text: "Ok~ You successfully post a comment.",
                    type: "success",
                    confirmButtonText: "OK"
                });
            }else{
                swal({
                    title: "Sorry, please rate it",
                    text: "Sorry, please rate it",
                    type: "warning",
                    confirmButtonText: "OK"
                });
            }
            },function(err){
                console.log(err);
        }); 
    },
    /**
    * render() render the module to the front-end
    */
    render: function() {

        var courseInfo = this.props.courseInfo;
        var enrollmentId = this.props.enrollmentId;

        const ratingStyle = {
            marginRight: '3px',
            float: 'right'
        }
        
        return (
            
            <form onSubmit={this.postComment} style={{width: "100%"}}>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <strong>
                            {this.state.username}
                        </strong>
                        <Rater style={ratingStyle} onRate={this.onRate}/>
                    </div>
                    <div className="panel-body">
                        <div className="form-group">
                            <textarea className="form-control"  onChange={this.onTextChange}/>
                        </div>
                        <button type="submit" className="btn btn-outline-primary btn-sm form-group">Submit</button>
                    </div>
                </div>
            </form>
         );
    }
});

module.exports = Comment_form;