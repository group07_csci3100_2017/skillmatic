var React = require('react');
import {withGoogleMap, GoogleMap, Marker} from 'react-google-maps';
import SearchBox from 'react-google-maps/lib/places/SearchBox';


const INPUT_STYLE = {
  boxSizing: `border-box`,
  MozBoxSizing: `border-box`,
  border: `1px solid transparent`,
  width: `240px`,
  height: `32px`,
  marginTop: `27px`,
  padding: `0 12px`,
  borderRadius: `1px`,
  boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
  fontSize: `14px`,
  outline: `none`,
  textOverflow: `ellipses`,
};

const GettingStartedGoogleMap = withGoogleMap(function(props){
  if(props.isIns) {
    return <GoogleMap
      ref={props.onMapMounted}
      defaultZoom={13}
      center={props.center}
      onBoundsChanged={props.onBoundsChanged}
      onKeyPress={props.handleEnter}
    >
      <SearchBox
        ref={props.onSearchBoxMounted}
        bounds={props.bounds}
        controlPosition={google.maps.ControlPosition.TOP_LEFT}
        onPlacesChanged={props.onPlacesChanged}
        inputPlaceholder="Customized your placeholder"
        inputStyle={INPUT_STYLE}
      />
      {props.markers.map((marker, index) => (
        <Marker position={marker.position} key={index} />
      ))}
    </GoogleMap>
  }else{

    return (<GoogleMap
      ref={props.onMapMounted}
      defaultZoom={13}
      center={props.center}
      onBoundsChanged={props.onBoundsChanged}
    >
      {props.markers.map((marker, index) => (
        <Marker position={marker.position} key={index} />
      ))}
    </GoogleMap>);
  }

});


var Venue_map = React.createClass({
    getInitialState: function() {
      if(this.props.lat){
        return {
              bounds: null,
              center: {
                lat: this.props.lat+0.031,
                lng: this.props.lng-0.04,
              },
              markers: [{position: {lat: this.props.lat, lng: this.props.lng}}]
            }
      }else{
        return {
              bounds: null,
              center: {
                lat: 47.6205588,
                lng: -122.3212725,
              },
              markers: []
            }
      }
    }, 

    handleMapMounted: function(map) {
      this._map = map;
    },

    handleBoundsChanged: function() {
      this.setState({
        bounds: this._map.getBounds(),
        center: this._map.getCenter(),
      });
    },

    handleSearchBoxMounted: function(searchBox) {
      this._searchBox = searchBox;
    },

    handleEnter: function(e) {
        if (e.key == 'Enter'){
            e.preventDefault();
            return false;
        }
        return true;
    },

    handlePlacesChanged: function() {
      const places = this._searchBox.getPlaces();

      // Add a marker for each place returned from search bar
      var markers = places.map(place => ({
        position: place.geometry.location,
      }));

      if(markers.length > 0){
        markers = [markers[0]];
      }

      // Set markers; set map center to first search result
      const mapCenter = markers.length > 0 ? markers[0].position : this.state.center;

      this.setState({
        center: mapCenter,
        markers,
      });
    
      this.props.onPlacesChanged(this.state.center);

    },

  render: function() {
    return (
      <div style={{height: `300px`}}>
        <GettingStartedGoogleMap
          containerElement={
            <div style={{ height: `300px`}} />
          }
          mapElement={
            <div style={{ height: `300px` }} />
          }
          center={this.state.center}
          onMapMounted={this.handleMapMounted}
          onBoundsChanged={this.handleBoundsChanged}
          onSearchBoxMounted={this.handleSearchBoxMounted}
          bounds={this.state.bounds}
          onPlacesChanged={this.handlePlacesChanged}
          markers={this.state.markers}
          isIns={this.props.isIns}
        />
      </div>
  )}


});


module.exports = Venue_map; 
