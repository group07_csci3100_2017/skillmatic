var React = require('react');
var Comments = require('Comments');
var Comment_form = require('Comment_form');
var moment = require('moment');
var Venue_map = require('Venue_map');
var cookie = require('react-cookie');
var Session_form = require('Session_form');
var Course_api = require('Course_api');
var Course_information = React.createClass({
    /**
    * renderBadge() we render the badges to the frontend
    * @category this is the array containing the categories
    */
    renderBadge:function(category){
        var forarray = [];
        category.map(function(cat){
            forarray.push(<span className="badge badge-info badge-pill" style={{marginLeft: '5px' }}>{cat}</span>)
        })
        return (<div>{forarray}</div>)
    },
    /**
    * renderCourse() we render the courses to the frontend
    * @courses this is the array containing the courses
    */
    renderCourse: function(courses){
        var forarray = [];
        if(courses){
            courses.map(function(cs){
                forarray.push(<span className="badge badge-danger badge-pill" style={{marginLeft: '5px' }}>{cs.title}</span>)
                
            })
            return (<div>{forarray}</div>)
        }else{
            return null;
        }
    },
    /**
    * enrollSession() we enroll session by parameter (session, title)
    * @session the session user want to enroll
    * @title this is the title the user want to enroll 
    */
    enrollSession: function(session, title) {
        var that = this;
        Course_api.enrollSession(session._id).then(function(res){
            console.log(session);
            that.props.rerender();
             swal({
                    title: "Successfully Enroll A Session",
                    text: "Congratulation! You have enrolled in " + title,
                    type: "success",
                    confirmButtonText: "OK"
                });
        }, function(err){
            console.log(err);
        });
    },
    /**
    * renderSession() reder the session of user's enrollment
    * @cards the cards representing the sessions
    * @ext the small revision of the data-taget
    */
    renderSession: function(cards, ext){
        if(cookie.load('username')){
        var list = []
        var that = this;
        cards.sessions.map(function(session) {
            if (ext === 'browse'){
                list.push(
                    // <form onSubmit={() => {that.enrollSession(session)}}>
                        <div className="collapse-group margin-list" key={session._id+ext + 'haha'}>
                            <button className="btn btn-primary btn-block" onClick={that.magicResize} data-toggle="collapse" data-target={"#detail"+session._id+ext}>
                                {moment(session.classDateTime).format("dddd, MMMM Do YYYY, h:mm a")}
                            </button>
                            <div className="collapse" id={"detail" + session._id + ext}>
                                <dl className="row" style={{margin: "auto"}}>
                                    <dt className="col-sm-3">Venue</dt>
                                    <dd className="col-sm-9">{session.venue}</dd>
                                </dl>
                                <Venue_map lng={session.lng} lat={session.lat}/>
                                <button style={{marginTop: "5px"}} onClick={(e) => {e.preventDefault; that.enrollSession(session, cards.title)}} className="btn btn-secondary">Enroll</button>
                           </div>
                        </div>
                    // </form>
                );
            }else {
                list.push(
                    <div className="collapse-group margin-list" key={session._id + ext + 'yoyo'}>
                        <button className="btn btn-primary btn-block" onClick={that.magicResize} data-toggle="collapse" data-target={"#detail"+session._id+ext}>
                            {moment(session.classDateTime).format("dddd, MMMM Do YYYY, h:mm a")}
                        </button>
                        <div className="collapse" id={"detail" + session._id + ext}>
                            <dl className="row" style={{margin: "auto"}}>
                                <dt className="col-sm-3">Venue</dt>
                                <dd className="col-sm-9">{session.venue}</dd>
                            </dl>
                            <Venue_map lng={session.lng} lat={session.lat}/>
                        </div>
                    </div>
                );
            }
        })
        return list;
        }else{
            return null;
        }
    },
    /**
    /* render the whole module
    */
    render:function(){
        var cards = this.props.cards;
        if(this.props.isEnroll === 'not' && cards){
            if(cards.detailins){
            var time = new Date(cards.createdAt);
            return(
            <div className="modal fade" id={cards._id} tabIndex="-1" role="dialog" aria-labelledby={cards._id + 'enroll'} aria-hidden="true">
                            <div className="modal-dialog modal-lg" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id={cards._id}>{cards.title}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body container" >
                                        {/*The course description: {cards.}*/}
                                        <dl className="row" style={{maxHeight: "400px"}}>
                                        <img src={"image/"+cards.profile_image.split('/')[2]}  className="img-thumbnail rounded float-left" style={{maxWidth:'90%', margin:'auto', maxHeight:"inherit"}}/>
                                        </dl>
                                        
                                        <dl className="row dl-horizontal" style = {{width:'90%', margin:'0 auto'}}>
                                        <dt className = "col-sm-3">Categories: </dt>
                                        <dd className = "col-sm-9">{this.renderBadge(cards.categories)}</dd>
                                        <dt className="col-sm-3">The course fee:</dt>
                                        <dd className="col-sm-9"> {cards.fee} HKD</dd>

                                        <dt className="col-sm-3">Description:</dt>
                                        <dd className="col-sm-9">{cards.description}</dd>

                                        <dt className="col-sm-3">Duration</dt>
                                        <dd className="col-sm-9">{cards.duration} min</dd>

                                        <dt className="col-sm-3">Created at</dt>
                                        <dd className="col-sm-9">{time.toLocaleString()}</dd>

                                        <dt className="col-sm-3">Instructor:</dt>
                                        <dd className="col-sm-9">{cards.instructor.username}</dd>
                                
                                        <dt className="col-sm-3">Courses he held:</dt>
                                        <dd className="col-sm-9">{this.renderCourse(cards.detailins.coursesHeld)}</dd>

                                        <dt className="col-sm-3">Max class size:</dt>
                                        <dd className="col-sm-9">{cards.maxClassSize}</dd>
                                        </dl>


                                        <dl className="row" style = {{width:'90%', margin:'0 auto'}}>
                                    {this.renderSession(cards, 'browse')}
                                    <Comments comments={cards.comments}/>
                                        </dl>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
        );
            }else{
                var time = new Date(cards.createdAt);
                return (
                    <div className="modal fade" id={cards._id + 'ins'} tabIndex="-1" role="dialog" aria-labelledby={cards._id} aria-hidden="true">
                            <div className="modal-dialog modal-lg" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id={cards._id + 'ins'}>{cards.title}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div className="modal-body" >
                                        {/*The course description: {cards.}*/}
                                        <dl className="row" style={{maxHeight: "400px"}}>
                                        <img src={"image/"+cards.profile_image.split('/')[2]} onClick={this.magicResize} className="img-thumbnail rounded float-left" style={{maxWidth:'90%', margin:'auto', maxHeight:"inherit"}}/>
                                        </dl>
                                        
                                        <dl className="row" style = {{width:'90%', margin:'0 auto'}}>
                                        <dt className = "col-sm-3">Categories: </dt>
                                        <dd className = "col-sm-9">{this.renderBadge(cards.categories)}</dd>
                                        <dt className="col-sm-3">The course fee:</dt>
                                        <dd className="col-sm-9"> {cards.fee} HKD</dd>

                                        <dt className="col-sm-3">Description:</dt>
                                        <dd className="col-sm-9">{cards.description}</dd>

                                        <dt className="col-sm-3">Duration</dt>
                                        <dd className="col-sm-9">{cards.duration} min</dd>

                                        <dt className="col-sm-3">Created at</dt>
                                        <dd className="col-sm-9">{time.toLocaleString()}</dd>

                                        <dt className="col-sm-3">Max class size:</dt>
                                        <dd className="col-sm-9">{cards.maxClassSize}</dd>
                                        </dl>

                                         <dl className="row" style = {{width:'90%', margin:'0 auto'}}>
                                        {this.renderSession(cards, 'ins')}
                                        <Session_form courseId={cards._id}/>
                                        <Comments comments={cards.comments}/>
                                        </dl>
                                        
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                );
            }
        }else if (cards){
            var time = new Date(cards.createdAt);
            return (
            
            <div className="modal fade" id={cards._id + "enroll"} tabIndex="-1" style={{"overflow-y": "scroll"}} role="dialog" aria-labelledby={cards._id} aria-hidden="true">
                            <div className="modal-dialog modal-lg"  role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id={cards._id + "enroll"}>{cards.title}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div className="modal-body" >
                                        {/*The course description: {cards.}*/}
                                        <dl className="row" style={{maxHeight: "400px"}}>
                                        <img src={"image/"+cards.profile_image.split('/')[2]}  className="img-thumbnail rounded float-left" style={{maxWidth:'90%', margin:'auto', maxHeight:"inherit"}}/>
                                        </dl>
                                        
                                        <dl className="row" style = {{width:'90%', margin:'0 auto'}}>
                                        <dt className = "col-sm-3">Categories: </dt>
                                        <dd className = "col-sm-9">{this.renderBadge(cards.categories)}</dd>
                                        <dt className="col-sm-3">The course fee:</dt>
                                        <dd className="col-sm-9"> {cards.fee} HKD</dd>

                                        <dt className="col-sm-3">Description:</dt>
                                        <dd className="col-sm-9">{cards.description}</dd>

                                        <dt className="col-sm-3">Duration</dt>
                                        <dd className="col-sm-9">{cards.duration} min</dd>

                                        <dt className="col-sm-3">Created at</dt>
                                        <dd className="col-sm-9">{time.toLocaleString()}</dd>

                                        <dt className="col-sm-3">Instructor:</dt>
                                        <dd className="col-sm-9">{cards.detailins.username}</dd>

                                        <dt className="col-sm-3">Courses he held:</dt>
                                        <dd className="col-sm-9">{this.renderCourse(cards.detailins.coursesHeld)}</dd>
                                
                                        <dt className="col-sm-3">Max class size:</dt>
                                        <dd className="col-sm-9">{cards.maxClassSize}</dd>

                                        </dl>
                                         <dl className="row" style = {{width:'90%', margin:'0 auto'}}>
                                        {this.renderSession(cards, 'enroll')}
                                        <Comments comments={cards.comments}/>
                                        <Comment_form enrollmentId={this.props.enrollmentId} refresh = {this.props.refresh}/>
                                        </dl>
                                    </div>

                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
        );
        }else{
            return (
                <div className="modal fade" id={cards._id} tabIndex="-1" role="dialog" aria-labelledby={cards._id} aria-hidden="true">
                            <div className="modal-dialog modal-lg" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id={cards._id}>{cards.title}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        {this.renderBadge(cards.categories)}
                                        {/*The course description: {cards.}*/}
                                        <img src={"image/"+cards.profile_image.split('/')[2]}  className="img-thumbnail rounded float-left" style={{width: '30%', margin: '20px'}}/>
                                        {cards.description}
                                        <Comments comments={cards.comments}/>
                                        <Comment_form enrollmentId={this.props.enrollmentId}/>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" className="btn btn-primary" >Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
            )
        }
    }
});
module.exports = Course_information;
    
