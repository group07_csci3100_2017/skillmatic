var React = require('react');
var Course_cards = require('Course_cards');
var Course_api = require('Course_api');
var Course_create = require('Course_create');
const Slider = require('rc-slider');
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
import css from 'rc-slider/assets/index.css';

var Course = React.createClass({
    /**
    * we set the initial state by calling the api Course_api.getCoursesby[every class inside]
    */
    getInitialState:function(){
         if(this.props.keyword === null || this.props.keyword === undefined){
            var that = this;
            Course_api.getCoursesby(['Sports','Music','DIY','Arts','Others','Cooking']).then(function(res){
                var temp = res;
                if(temp[0]){
                temp.map(function(courses){
                    Course_api.getInstructor(courses.instructor._id).then(function(value){
                        courses.detailins = value;
                        that.setState({courses_review:temp});
                    })
                })}else{
                that.setState({courses_review:null});
                }
            }
        ,function(err){
            console.log(err);
        })}else{
            var that = this;
            Course_api.getCoursesby(['Sports','Music','DIY','Arts','Others','Cooking'],that.props.keyword,1440,0,9999,0).then(function(res){
                var temp = res;
                if(temp[0]){
                temp.map(function(courses){
                    Course_api.getInstructor(courses.instructor._id).then(function(value){
                        courses.detailins = value;
                        that.setState({courses_review:temp});
                    })
                })}else{
                that.setState({courses_review:null});
                }
            }
        ,function(err){
            console.log(err);
        })
        }
        return {
            keyword:'',
            courses:[],
            course_selected:[],
            durationMax:1440,
            durationMin:0,
            feeMax:9999,
            feeMin:0
        }
    },
    handleAnotherSearchBar: function(){

    },
    //after any setState function is called below, we execute the handleRerender.
    /**
    * if the keywords input change we call this handleKeywords function
    * handleKeywords() this will set State.keyword. 
    */
    handleKeywords:function(e){
        this.setState({keyword:e.target.value});
        this.handleRerender();
    },
    /**
    * if the Check input change we call this handleCheck function
    * handleCheck() this will set State.course_selected. 
    */
    handleCheck:function(e){
        var temp = this.state.course_selected;
            if(e.target.checked === true){
                temp.push(e.target.value);
                this.setState({course_selected:temp}, this.handleRerender);
            }else{
                var i = temp.indexOf(e.target.value);
                if(i != -1) {
                    temp.splice(i, 1);
                }
                this.setState({course_selected:temp},this.handleRerender);
            }

    },
    // getCoursesby: function(category,keyword,durationMax,durationMin,feeMax,feeMin){
    /**
    * if the FeeBar input change we call this handleFeeBar function
    * handleFeeBar() this will set State.course_selected. 
    * @feeMin: fee minimum critiria
    * @feeMax: fee maximum critiria
    */        
    handleFeeBar:function(value){
        var feeMin = value[0];
        var that = this;
        var feeMax = value[1];
        this.setState({feeMin:feeMin, feeMax:feeMax},this.handleRerender);
    },
    /**
    * if the FeeBar input change we call this handleFeeBar function
    * handleDurationBar() this will set State.course_selected. 
    * @feeMin: feeMin 
    */   
    handleDurationBar:function(value){
        var durationMin = value[0]*60;
        var durationMax = value[1]*60;
        this.setState({durationMin:durationMin,durationMax:durationMax},this.handleRerender)
    },
    /*
    * handleRerender() this will 
    */
    handleRerender: function(){
        var that = this;
        console.log(this.state.durationMin);
        Course_api.getCoursesby(this.state.course_selected,this.state.keyword,this.state.durationMax,this.state.durationMin,this.state.feeMax,this.state.feeMin).then(function(res){
            var temp = res;
            if(temp[0]){
            temp.map(function(courses){
                Course_api.getInstructor(courses.instructor._id).then(function(value){
                    courses.detailins = value;
                    that.setState({courses_review:temp});
                })
            })}else{
              that.setState({courses_review:null});
            }
        },function(err){
            console.log(err);
        })
    },
    /*
    * render() render the whole module 
    */
    render:function(){
        const jumbotronback = {
            backgroundImage: 'url('+"./images/background-1.png"+')',
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            backgroundSize:'cover'
        };
        return(
            <div>
        <div >

           <div className="jumbotron jumbotron-fluid divparagraph" style = {jumbotronback}>
              
                <div className="container">
                    
                    <div className="container row">
                <div className="form-check form-check-inline col">
                    <label className="form-check-label">
                        <input className="form-check-input" type="checkbox"  id="inlineCheckbox1" value="Sports" onChange={this.handleCheck}/> Sports
                    </label>
                </div>
                <div className="form-check form-check-inline col">
                    <label className="form-check-label">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="DIY" onChange={this.handleCheck}/> DIY
                    </label>
                </div>
                <div className="form-check form-check-inline col">
                    <label className="form-check-label">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="Music" onChange={this.handleCheck}/> Music
                    </label>
                </div>
                <div className="form-check form-check-inline col">
                    <label className="form-check-label">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="Cooking" onChange={this.handleCheck}/> Cooking
                    </label>
                </div>
                <div className="form-check form-check-inline col">
                    <label className="form-check-label">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="Arts" onChange={this.handleCheck}/> Arts
                    </label>
                </div>
                <div className="form-check form-check-inline col">
                    <label className="form-check-label">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="Others" onChange={this.handleCheck}/> Others
                    </label>
                </div>
                </div >
                <br/>
                <div className = "input-group">                   <input type="text" className="form-control inline" id = "keyword" placeholder="Keyword" aria-describedby="sizing-addon2"  onChange={this.handleKeywords}/>
                                                          </div>
                                                          <br/>
                             <p>Duration</p>
                             <Range min={0} max = {1440/60} defaultValue={[0,1440/60]} onChange={this.handleDurationBar}/>
                             <br/>
                             <p>Course Fee</p>
                             <Range min={0} defaultValue={[0,9999]} max = {9999} onChange={this.handleFeeBar}/>
                </div>

           </div>
           <Course_create reallyrender={this.props.loginStatus} rendertype = 'body' afterSubmit = {this.handleRerender}/>
           <Course_cards rerender = {this.props.getUserEnrollment} cardsInfo={this.state.courses_review} isContainer = 'true'/>
        </div>
           <Course_create reallyrender={this.props.loginStatus} rendertype = 'button' afterSubmit = {this.handleRerender}/>

        </div>
        );
    }
}   
);
module.exports = Course;
    
