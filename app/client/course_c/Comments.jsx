var React = require('react');
var Course_api = require('Course_api');
var cookie = require('react-cookie');

var moment = require('moment');
import Rater from 'react-rater';

var Comments = React.createClass({
    /**
    * getInitialState() set the comments as same as props.comments.
    */
    getInitialState: function() {
        return {comments: this.props.comments};
    }, 
    /**
    * renderEditButton() this function will render the editButton.
    */
    renderEditButton: function(username){
        // if(cookie.load('username') === username){
        //     return( <button className = "btn btn-primary btn-sm">edit</button>)
        // }
    },
    /**
    * render() this function will render whole module.
    */
    render: function() {
        var that = this;
        var cmts = this.props.comments;
        const ratingStyle = {
            marginRight: '3px',
            float: 'right'
        }

        return (
            <div style={{width: "100%"}}>
            {cmts.map(function(cmt){
                return (
                    <div key={cmt._id} className="panel panel-default">
                        <div className="panel-heading">
                            <strong>
                                 {'>'+cmt.studentUsername+'    '} 
                            </strong>
                            {that.renderEditButton(cmt.studentUsername)}
                            <span style={{marginLeft: "10px"}}>
                            {moment(cmt.commentDateTime).format("MMMM Do, h:mm a")}
                            </span>
                            <Rater rating={cmt.rating} style={ratingStyle} interactive={false}/>
                        </div>
                        <div className="panel-body">
                            {cmt.text}
                        </div>
                    </div>
                    );
            })}
            </div>
         );
    }
});

module.exports = Comments;