var React = require('react');
var Course_api = require('Course_api');

import Draggable, {DraggableCore} from 'react-draggable'; // Both at the same time
var Course_create = React.createClass({
    /**
    * @formData this is a object to store the form data
    */
    getInitialState: function(){
        return {
            formData : new FormData(),
        }
    },
    /**
    * after the submit butoon being pressed, the frontend will sent the @formData to Course_api.createNewCourse
    * @formData this is a object to store the form data
    */
    createCourse:function(e){
        e.preventDefault();
        var that = this;
        Course_api.createNewCourse(this.state.formData).then(function(res){
          that.props.afterSubmit();
          swal({
                    title: "Successfully Create A Course",
                    text: "Ok~ You successfully create a course",
                    type: "success",
                    confirmButtonText: "OK"
                });
        },function(err){
            console.log(err);
        })
    },
    /**
    * onTitleChange after the Title inputbox being changed we'll the formData store in the state.
    * @formData this is a object to store the form data
    * @formData.title will be updated.
    */
    onTitleChange:function(e){
        var temp = this.state.formData;
        temp.delete('title');
        temp.append('title',e.target.value);
        this.setState({formData:temp});
    },
    /**
    * onFeeChange after the Fee inputbox being changed we'll the formData store in the state.
    * @formData this is a object to store the form data
    * @formData.fee will be updated
    */
    onFeeChange:function(e){
        var temp = this.state.formData;
        temp.delete('fee');
        temp.append('fee',e.target.value);
        this.setState({formData:temp});
    },
    /**
    * onDurationChange after the duration inputbox being changed we'll the formData store in the state
    * @formData this is a object to store the form data
    * @formData.duration will be updated.
    */
    onDurationChange:function(e){
        var temp = this.state.formData;
        temp.delete('duration');
        temp.append('duration',e.target.value);
        this.setState({formData:temp});
    },
    /**
    * onMaxClassSizeChange after the maxClassSize inputbox being changed we'll the formData store in the state
    * @formData this is a object to store the form data
    * @formData.maxClassSize will be updated.
    */
    onMaxClassSizeChange:function(e){
        var temp = this.state.formData;
        temp.delete('maxClassSize');
        temp.append('maxClassSize',e.target.value);
        this.setState({formData:temp});
    },
    /**
    * onDescriptionChange after the Descriptionn inputbox being changed we'll the formData store in the state
    * @formData this is a object to store the form data
    * @formData.Description will be updated.
    */
    onDescriptionChange:function(e){
        var temp = this.state.formData;
        temp.delete('description');
        temp.append('description',e.target.value);
        this.setState({formData:temp});
    },
    /**
    * handleSelectedChange after the select being changed we'll the formData store in the state
    * @formData this is a object to store the form data
    * @formData.categories will be updated.
    */
    handleSelectedChange: function(e) {
        var options = e.target.options;
        var value = '';
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                if(i===0){
                    value = value+ options[i].value;
                }else{
                    value = value +',' + options[i].value;
                }
            }
        }
        var temp = this.state.formData;
        temp.delete('categories');
        temp.append('categories',value);
        this.setState({formData:temp});
    },
    /**
    * onFileUpload after the fileupload being changed we'll the formData store in the state
    * @formData this is a object to store the form data
    * @formData.profile_image will be updated.
    */
    onFileUpload:function(e){
        var file = e.target.files[0];
        var temp = this.state.formData;
        temp.delete('profile_image');
        temp.append('profile_image',file);
        this.setState({formData:temp});
    },
    /**
    * render the front end module.
    */
    render:function(){
        if(this.props.reallyrender !== 'notlogin'){
        if(this.props.rendertype === 'button'){
            return(
                    <Draggable
                        handle=".handle"
                        zIndex={999999}
                        onStart={this.handleStart}
                        onDrag={this.handleDrag}
                        onStop={this.handleStop}>
                        <div className="handle" style={{position: 'absolute', bottom: '50px', right: '25px'}}>
                            <button className = "btn btn-danger" data-toggle="modal" data-target={"#createCourse"}>+</button>
                        </div>
                    </Draggable>
            )
        }else if (this.props.rendertype === 'body'){
            return (
            <div className="modal fade" id="createCourse" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Create a course</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form onSubmit={this.createCourse}>
                    <div className="modal-body">
                            <div className="form-group">
                                <label htmlFor="coursetitle">Course Title</label>
                                <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="courseHelp" placeholder="Enter Course Title" onChange={this.onTitleChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="fee">Fee</label>
                                <input type="number" min="0" className="form-control" id="exampleInputPassword1" placeholder="Enter Course Fee" onChange={this.onFeeChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="duration">Course Duration</label>
                                <input type="number" min="0" className="form-control" id="exampleInputPassword1" placeholder="Enter Course Duration" onChange={this.onDurationChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="maxClassSize">Max className Size</label>
                                <input type="number" min="0" className="form-control" id="exampleInputPassword1" placeholder="Enter Max Class Size" onChange={this.onMaxClassSizeChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleSelect1">Category of Course</label>
                                <select className="form-control" id="Category" multiple="multiple" onChange={this.handleSelectedChange}>
                                <option value="Sports" selected="selected">Sports</option>
                                <option value="DIY">DIY</option>
                                <option value="Cooking">Cooking</option>
                                <option value="Arts">Arts</option>
                                <option value="Music">Music</option>
                                <option value="Others">Others</option>
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleTextarea">Course Description</label>
                                <textarea className="form-control" id="exampleTextarea" rows="3" onChange={this.onDescriptionChange}></textarea>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputFile">Course Image</label>
                                <input type="file" className="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" onChange={this.onFileUpload}/>
                                <small id="fileHelp" className="form-text text-muted">We supports png and jpeg file.</small>
                            </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" className="btn btn-primary"  >Create Course</button>
                    </div>
                    </form>
                    </div>
                </div>
            </div>)
        }else{
        return (null)
        }
    }else{
        return (null)
    }
    }
}   
);
module.exports = Course_create;
    
