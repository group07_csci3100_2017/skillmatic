var React = require('react');
var Slider = require('react-slick');
var {Link} = require('react-router');
//this is the static footer for each module
var Footer = React.createClass({
    fbShare: function(site, url, title, descr, winWidth, winHeight){
      const fbURL = 'http://www.facebook.com/sharer.php?s=100&p[title]=';
      const twitterURL = 'https://twitter.com/intent/tweet?text=';
      const googleURL = 'https://plus.google.com/share?url=';

      var winTop = (screen.height / 2) - (winHeight / 2);
      var winLeft = (screen.width / 2) - (winWidth / 2);
      if (site === 'fb')
        var sharedURL = fbURL+title+'&p[summary]='+descr+'&p[url]='+url;
      else if (site === 'twitter') 
        var sharedURL = twitterURL+descr+'&url='+url;
      else if (site === 'google')
        var sharedURL = googleURL+url;
      window.open(sharedURL, 'sharer', 'top='+winTop+',left='+ winLeft+',toolbar=0,status=0,width='+winWidth+',height='+winHeight);
    },
    render: function(){
        const fbImg = 'https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png';
        const twitterImg = 'https://g.twimg.com/dev/documentation/image/Twitter_logo_blue_48.png';
        const googleImg = 'https://www.gstatic.com/images/icons/gplus-64.png';

        const bg1 = {
            backgroundColor: '#474e5d',
            color: 'bfc9cc'
        };
        const contact = {
          textAlign: 'left', 
          paddingLeft: '30px', 
          paddingTop: '10px', 
          fontSize: '14px'
        };
        const rightCol = {
          textAlign: 'right',
          paddingRight: '30px'
        };
        return(
            <div className="container-fluid text-center" style = {bg1}>
              <div className='row' style={{paddingTop: '15px'}}>
                <div className='col-md-8' style={contact}>
                  Address: The Chinese University of Hong Kong, Shatin, NT, HK <br/>
                  Tel: (852) 9876 5432
                </div>
                <div className='col-md-4' style={rightCol}>
                  <div>
                  <Link to='/about'>About</Link> | <Link to='/course'>Courses</Link>
                  </div>
                  <img src={fbImg} width='30px' height='30px' onClick={this.fbShare.bind(this, 'fb', 'google.com', 'Skillmatic fb Share', 'Check out what courses there are in Skillmatic!', 520, 350)} style={{ cursor: 'pointer', marginTop: '10px', marginRight: '10px' } }/> 
                  <img src={twitterImg} width='30px' height='30px' onClick={this.fbShare.bind(this, 'twitter', 'google.com', 'Skillmatic twitter Share', 'Check out what courses there are in Skillmatic!', 520, 350)} style={{ cursor: 'pointer', marginTop: '10px', marginRight: '10px', backgroundColor: 'white', borderRadius: '2px' } }/>
                  <img src={googleImg} width='30px' height='30px' onClick={this.fbShare.bind(this, 'google', 'google.com', '', '', 520, 350)} style={{ cursor: 'pointer', marginTop: '10px' } }/> <br/>
                </div>
              </div>
              <div style={{paddingBottom: '10px'}}>
                © Skillmatic 2017
              </div>
            </div>
        );
    }
});
module.exports = Footer;
