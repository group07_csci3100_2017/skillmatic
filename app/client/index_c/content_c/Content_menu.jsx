var React = require('react');
var Content_cards = require('Content_cards');
var Course_api = require('Course_api');
var Scroll  = require('react-scroll');
var scroller = Scroll.scroller;
var Element = Scroll.Element;
var Content_menu = React.createClass({
    /*
    * getInitialState() we use this function to set the initial state @style, @topic and @courses
    * @style we set the padding and width ... for the state.
    * @topic we initialize the topic to be 'Sports'.
    * @courses we set the title, instructor... all equal to loading
    */
    getInitialState: function(){
        this.onListClick('Sports');
        return {
            style:{width: '100%', paddingRight: '0px', paddingLeft: '0px', backgroundImage:'url('+"/images/menu-1.png"+')', marginBottom: '30px'},
            topic: 'Sports',
            courses: [{title:'Loading', instructor:'Loading',email:'Loading',selfintro:'Loading', image: './images/Life.png'},{title:'Loading', instructor:'Loading',email:'Loading',selfintro:'Loading', image: './images/Life.png'},{title:'HI', instructor:'Loading',email:'Loading',selfintro:'Loading', image: './images/Life.png'}]
        }
    },
    /*
    * onListClick() this function will be invoked whenever List is be clicked
    * @category we will accept the category and print it out
    */
    onListClick:function(category){
        this.handleTopicChange(category);
    },
    /* 
    * we call the api from Course_api.getInstructor to get the instructor and iterate it 3 times
    */
    callapi: function(i, courses){
        var that = this;
        Course_api.getInstructor(courses[i].instructor._id).then(function(instructor){
                    var username = instructor.username;
                    var useremail = instructor.email;
                    var selfintro = instructor.coursesHeld;
                    var thecourses = that.state.courses.slice();
                    thecourses[i].instructor = username;
                    thecourses[i].email = useremail;
                    thecourses[i].coursesHeld = instructor.coursesHeld;
                    thecourses[i].title = courses[i].title;
                    thecourses[i].image = courses[i].profile_image;
                    thecourses[i].itself = courses[i];
                    console.log(thecourses[i]);
                    that.setState({courses:thecourses});
                    if(i < 2){
                        that.callapi(i+1,courses);
                    }
                });
        
        
    },
    /* 
    * handleTopicChange() whenever the topic is changed we will set the topic state as the input category
    * and we also call the Course_apo to get the courses by the category we input
    */
    handleTopicChange: function(category) {
        var that = this;
        that.setState({
                topic: category,
        });
        Course_api.getCoursesbyCat([category]).then(function (courses){
            var i = 0;
            that.callapi(i,courses);
        }, function (errorMessage){
        })
    },
    /* 
    * handleColor() this will handle the background
    */
    handleColor: function(category){
        var old_style = this.state.style;
        if(this.state.topic === 'Sports'){
            old_style.backgroundImage = 'url('+"/images/menu-2.png"+')'
        }else if (this.state.topic === 'DIY'){
            old_style.backgroundImage = 'url('+"/images/menu-1.png"+')'
        }else if (this.state.topic === 'Music'){
            old_style.backgroundImage = 'url('+"/images/menu-4.png"+')'
        }else if (this.state.topic === 'Cooking'){
            old_style.backgroundImage = 'url('+"/images/menu-3.png"+')'
        }else if (this.state.topic === 'Arts'){
            old_style.backgroundImage = 'url('+"/images/menu-5.png"+')'
        }else{  
            old_style.backgroundImage = 'url('+"/images/menu-6.png"+')'
        }
        if(category === this.state.topic){
            return {
                cursor: "pointer",
                
            }
        }else{
            var ind = ["Sports", "DIY", "Music", "Cooking", "Arts", "Others"].indexOf(category);
            return {
                cursor: "pointer",
                backgroundColor: ["#d9d9d9", "#e6e6e6"][ind % 2]
            }
        }
            this.setState({style:old_style});

    },
    /* 
    * render() this will render the whole module
    */
    render:function(){
        const pointer  = {
            cursor: "pointer"
        };

        return(
            <div id="divmainMenu" className="container-fluid element backrelated" style={this.state.style}>
                    <ul className="nav nav-fill" >
                        <li  className="nav-item">
                            <a className="nav-link active" style = {this.handleColor('Sports')} onClick={this.onListClick.bind(this, 'Sports')} >
                                <div>
                                <img className="icon img-fluid" src='images/icons/sports.png'>
                                </img>
                                </div>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" style = {this.handleColor('DIY')} onClick={this.onListClick.bind(this, 'DIY')}>
                                
                                <div>
                                <img className="icon img-fluid" src='images/icons/DIY.png'>
                                </img>
                                </div>
                           </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" style = {this.handleColor('Music')} onClick={this.onListClick.bind(this, 'Music')}>
                                
                                <div>
                                <img className="icon img-fluid" src='images/icons/music.png'>
                                </img>
                                </div>
                                </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" style = {this.handleColor('Cooking')} onClick={this.onListClick.bind(this, 'Cooking')}>
                                
                                <div>
                                <img className="icon img-fluid" src='images/icons/cooking.png'>
                                </img>
                                </div>
                                </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" style = {this.handleColor('Arts')} onClick={this.onListClick.bind(this, 'Arts')}>
                                
                                <div>
                                <img className="icon img-fluid" src='images/icons/arts.png'>
                                </img>
                                </div>
                                </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" style = {this.handleColor('Others')} onClick={this.onListClick.bind(this, 'Others')}>
                                
                                <div>
                                <img className="icon" src='images/icons/others.png'>
                                </img>
                                </div>
                                </a>
                        </li>
                    </ul>
                     <div className = "invisibleDiv" style = {{textAlign:'center', fontSize: '70px', position: 'relative',transform: 'translateY(20%)' ,color: 'white',
    textShadow: '2px 2px 4px #000000'}}> {this.state.topic} </div>
                <Content_cards topic={this.state.topic} courses = {this.state.courses}/>
               
            </div>
        );
    }
}
);
module.exports = Content_menu;
    
