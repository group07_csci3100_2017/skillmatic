var React = require('react');
var Content_menu = require('Content_menu');
var Index_Slider = require('Index_Slider');
var Content_cards = require('Content_cards');
var Content = React.createClass({
    /*
    * this module will render whole structure of Content_menu and Index_Slider
    */
    render:function(){
        
        return(
           <div>
                <Index_Slider/>
                <Content_menu/>
                
           </div>
        );
    }
}
);
module.exports = Content;
    