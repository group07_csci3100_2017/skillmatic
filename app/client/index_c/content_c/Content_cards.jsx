var React = require('react');
var Card_youtube = require('Card_youtube');
var Course_api = require('Course_api');
var Content_cards = React.createClass({

    getDefaultProps: function(){
      return {
          topic: 'Sports'
      }  
    },
    /**
    * getInitialState() set the topic to props.
    * @topic will set to this.props.topic
    */
    getInitialState: function(){
        return {
            topic: this.props.topic
        }
    },
    /**
    * renderCourses() this will render the courses by badges
    * @courses will accept the courses and print it out by the form of badges
    */
    renderCourses: function(courses){
        if(courses !== null && courses !== undefined){
                console.log(courses);

            var forreturn = [];
            courses.map(function(cour){
                console.log(cour);
                forreturn.push(<span className="badge badge-info badge-pill" style={{marginLeft: '5px' }}>{cour.title}</span>)
            });
            return (<p>Courses she/he open: {forreturn}</p>);
        }else{
            return null;
        }
    },
    /**
    * renderInstructor() this will accept the email and instructor and courses
    * by accepting three parameters
    * @email which is the email of instructor
    * @instructor which is the name of insturctor
    * @courses which is the courses created by the instructor
    */
    renderInstructor: function(email,instructor,courses){
        var that = this;
        if(email){
        return (<div>
                <a  data-toggle="modal" data-target={"#"+instructor}>
                 Instructor: {instructor}
                </a>
                <div className="modal fade" id={instructor} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Teacher Information</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                        <a href={"mailto:"+email}> Email: {email}</a>
                        {that.renderCourses(courses)}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
          )
        }else{
            return(<p>loading</p>)
        }
    },
    /**
    * render the whole module
    */
    render:function(){
        const firstcardHeight = {
            height:"200px",
            width:"auto"
        };
        const widthofdiv={
            width:"70%"
        };
        const divHeight = {
            width:"100%",
            height:"40%"
        };
        var topic = this.props.topic;
        var courses = this.props.courses;
        // this.getInstructorName(courses[0].instructor);
        if(courses[0].itself){
        return(
        <div className = "container">
                {/*<br/><h2>Courses of {topic}</h2><br/>*/}
                {/*<div className = "col-md-12">
                <div className="card">
                    <h3 className="card-header">Featured</h3>
                    <div className="card-block">
                        <img style={firstcardHeight} src="./images/Life.png" className="rounded float-right img-fluid" alt="..."/>
                        <h4 className="card-title">{courses[0].title}</h4>
                        <p className="card-text">{courses[0].description}</p>
                        <a href="#" className="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                </div>*/}
                {/*<div className = "col-md-12"><br/></div>*/}
                <div className="row">
                <div className="col-lg-4">
                    <div className="card divparagraph">
                        <a data-toggle="modal" data-target="#firstCard">
                            <img className="card-img-top img-fluid" style = {{height: '200px'}} src={"image/"+courses[0].image.split('/')[2]} alt="Card image cap"/>
                        </a>
                        <div className="card-block">
                            <h4 className="card-title">{courses[0].title}</h4>
                                {this.renderInstructor(courses[0].email,courses[0].instructor,courses[0].couresesHeld)}
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="card divparagraph">
                        <img className="card-img-top img-fluid" style = {{height: '200px'}}  src={"image/"+courses[1].image.split('/')[2]}  alt="Card image cap"/>
                        <div className="card-block">
                            <h4 className="card-title">{courses[1].title}</h4>
                                {this.renderInstructor(courses[1].email,courses[1].instructor,courses[1].coursesHeld)}
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="card divparagraph">
                        <img className="card-img-top img-fluid" style = {{height: '200px'}}  src={"image/"+courses[2].image.split('/')[2]} alt="Card image cap"/>
                            <div className="card-block">
                                <h4 className="card-title">{courses[2].title}</h4>
                                {this.renderInstructor(courses[2].email,courses[2].instructor,courses[2].coursesHeld)}
                            </div>
                    </div>
                </div>
                
                </div>
            <br/>
            <div style = {divHeight} >
                    <Card_youtube topic = {topic}/>
                </div>  
        </div>
        
        
        );}else{
            return null
        }
    }
}
);
module.exports = Content_cards;
    
