    var React = require('react');
    var ReactPlayer = require('react-player');


    var Card_youtube = React.createClass({
            /*
            * getDefaultProps() we set the defaultprops as topic is equal to loading
            */
            getDefaultProps: function(){
                return{
                    topic: 'loading'
                }
            },
            /*
            * getYoutubeID() we use the switch case and decide which link of youtube video should be rendered
            * @topic the parameter will decide the link of the youtube
            */
            getYoutubeID: function(topic){
                switch(topic){
                    case 'Sports':
                        return 'VOPkI1THbxI';
                        break;
                    case 'DIY':
                        return 'ShlW5plD_40';
                        break;
                    default:
                        return 'KMihKmoYfe8';
                }
            },
            /*
            * getYoutubeID() we use the switch case and decide which link of img  should be rendered
            * @topic the parameter will decide the url of the img
            */
            getImagePath: function(topic){
                switch(topic){
                    case 'Sports':
                        return 'url('+"/images/Sports.jpg"+')';
                        break;
                    case 'DIY':
                        return 'url('+"/images/DIY.jpg"+')';
                        break;
                    case 'Music':
                        return 'url('+"/images/Music.jpg"+')';
                        break;
                    case 'Cooking':
                        return 'url('+"/images/Cooking.jpg"+')';
                        break;
                    case 'Arts':
                        return 'url('+"/images/Arts.jpg"+')';
                        break;
                    default:
                        return 'url('+"/images/Others.jpg"+')';
                        break;
                    
                }
            },
            /*
            * render() this will render whole module
            */
            render: function(){
                var topic = this.props.topic;
                var youtubeID = this.getYoutubeID(topic);
                var imagePath = this.getImagePath(topic);
                if(topic === 'loading'){
                    return <p>loading</p>
                }else{
                return(
                    <div className = "row">
                        <div className = "col-sm-6 " >
                        <ReactPlayer className = "divparagraph"url={'https://www.youtube.com/watch?v='+youtubeID} width="100%" height ="auto" />
                        </div>
                        <div className = "col-sm-6 divparagraph" style = {{ lineHeight: 'auto',verticalAlign: 'middle',
textAlign:'center', backgroundImage: imagePath}}>
                        </div>
                    </div>
                );}
            }
        }
    )
    module.exports = Card_youtube;
