var React = require('react');
var {Link} = require('react-router');
var Slider = require('react-slick');
var Scroll  = require('react-scroll');
var AnotherLink = Scroll.Link;
var Element    = Scroll.Element;
var Events     = Scroll.Events;
var scroll     = Scroll.animateScroll;
var scrollSpy  = Scroll.scrollSpy;
// code below is recommended by the documentation of slider 
// we just need to modify the parameter in render function
var Index_Slider = React.createClass({
    componentDidMount: function() {

    Events.scrollEvent.register('begin', function(to, element) {
    });

    Events.scrollEvent.register('end', function(to, element) {
    });

    scrollSpy.update();

  },
  componentWillUnmount: function() {
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  },
    render: function(){
        var settings = {
        infinite: true,
        fade:false,
        swipeToSlide:true,
        swipe: false,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        infinite: true,
        slidesToScroll: 1,
        autoplay:true,
        arrows:true
    };
        
        const divinstyle = {
            margin: 'auto',
        };
        const jumbotronback1 = {
            backgroundImage: 'url('+"./images/background-index-1.jpg"+')',
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            backgroundSize:'cover',
            borderRadius: "0px",
        
        };
        const jumbotronback2 = {
            backgroundImage: 'url('+"./images/background-index-2.jpg"+')',
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            backgroundSize:'cover',
            borderRadius: "0px"
        };
        const jumbotronback3 = {
            backgroundImage: 'url('+"./images/background-index-3.jpg"+')',
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            backgroundSize:'cover',
            borderRadius: "0px"
        };
        const jumbotronback4 = {
            backgroundImage: 'url('+"./images/background-index-4.jpg"+')',
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            backgroundSize:'cover',
            borderRadius: "0px"
        };
        const textshadow = {
            color : 'white',
            // textShadow:'11px 3px 7px rgba(0, 0, 0, 1)',
            fontSize: '70px'
        };
        const text = {
            color: 'white',
            // textShadow: '11px 3px 7px rgba(0, 0, 0, 1)'
        };
        const logoStyle = {
            margin: "auto", 
            padding: "20px"
        };
        const grey = {
            color: 'grey',
            
        };
        return(
            <div>
                
           <div className ="divstyle">
                <Slider {...settings}>
                    <div className="jumbotron" style={jumbotronback1}>
                        {/*<h1 style = {textshadow}>Skillmatic</h1>*/}
                        <a style ={{ display: 'block',marginLeft: 'auto,margin-right: auto'}}><img  className = "img-fluid" style={{margin: '0 auto', height: '30%'}} src="http://fontmeme.com/permalink/170411/3376241a8b41246c6cd2cb110a73724c.png" alt="calligraphy-fonts" border="0"/></a><br/><br/>
                        <div style={grey} className = "textindiv">
                            <p style={text}> E-Learning is a modern way to learn professional knowledge and skills through Internet.<br/> People can take various courses online to learn skills. <br/>
                               Nevertheless, some skills are difficult to learn by just watching videos online.<br/> Therefore, we built a sharing platform for people to teach and learn leisure skills by face-to-face instruction.</p>
                        </div>
                        <div style = {{height: '50px'}} className = "textindiv"></div>
                        <div>
                            <AnotherLink to = "test" spy={true} smooth={true} duration={500}><button className="btn btn-success">Start your jouney</button></AnotherLink>
                        </div>
                    </div>
                    <div className="jumbotron" style={jumbotronback2}>
                        {/*<h1 style = {textshadow}>Skillmatic</h1>*/}
                        <a style ={{ display: 'block',marginLeft: 'auto,margin-right: auto'}}><img  className = "img-fluid" style={{margin: '0 auto', height: '30%'}} src="http://fontmeme.com/permalink/170412/5f2e395cd491e2d74533e69925f392d6.png" alt="calligraphy-fonts" border="0"/></a><br/><br/>
                        <div style={grey} className = "textindiv">
                            <p style={text}>Borussia Dortmund said in a statement: "Shortly after the departure of the Borussia Dortmund team bus <br/>from the hotel to the stadium there was an incident. The bus has been damaged in two places."</p>
                        </div>
                        <br/>
                        <br/>
                        <div style={grey} className = "textindiv">
                            <p style={text}>Ms Bridges in her post said: "This man is a doctor and [said he had] to be at the hospital in the morning. He did <br/>not want to get off." She added that everyone on board was "shaky and so disgusted".</p>
                        </div>
                        <br/>
                        <br/>
                        <div style={grey} className = "textindiv">
                            <p style={text}>Outrage over the controversial incident soon spread to the Chinese internet, where thousands of shocked and angry netizens <br/>took to China's top micro-blogging site Sina Weibo to express their anger with the US airline.</p>
                        </div>
                    </div>
                    <div className="jumbotron" style={jumbotronback3}>
                        {/*<h1 style = {textshadow}>Skillmatic</h1>*/}
                        <a style ={{ display: 'block',marginLeft: 'auto,margin-right: auto'}}><img  className = "img-fluid" style={{margin: '0 auto', height: '30%'}} src="http://fontmeme.com/permalink/170412/d56c858dec944cd849be1b094972dbdd.png" alt="calligraphy-fonts" border="0"/></a><br/><br/>
                        <div style={grey} className = "textindiv">
                            <p style={text}>Miss AU Pik Kwan, Susanna (Chinese Medicine) said: "My five years of university life have nearly come to an end. I am so <br/>grateful that in these five years I have experienced many things, including an exchange in America and service <br/>trips, which broadened my horizons and challenged my limits. These experiences helped me to develop into a better person."</p>
                        </div>
                        <br/>
                        <br/>
                        <div style={grey} className = "textindiv">
                            <p style={text}>Mr CHOI Chun Ho, Joseph (Medicine) said: "In late January we lost our greatest teacher, Professor S F Leung, whom we used to greet<br/> and congratulate every year in this ceremony. Fai Sir, we shall always remember your sunny smiles and words of wisdom."</p>
                        </div>
                    </div>
                    <div className="jumbotron" style={jumbotronback4}>
                        {/*<h1 style = {textshadow}>Skillmatic</h1>*/}
                        <a style ={{ display: 'block',marginLeft: 'auto,margin-right: auto'}}><img  className = "img-fluid" style={{margin: '0 auto', height: '30%'}} src="http://fontmeme.com/permalink/170412/efb98372f4ae6a7ca1dd29f684e0c13a.png" alt="calligraphy-fonts" border="0"/></a><br/><br/>
                        <div style={grey} className = "textindiv">
                            <img style={{margin: '0 auto'}} src= "http://fontmeme.com/permalink/170412/5c3c809d5fb4aff58288e4003ee0f701.png"/>
                        </div>
                        <br/>
                        <br/>
                        <div style={grey} className = "textindiv">
                                <p style = {text}>Full-time teacher<br/>Full-time associate<br/>Analyst<br/>Intern</p>
                        </div>
                        <div style={grey} className = "textindiv">
                            <p style = {text}>Contact us through (852)98765432!</p>
                        </div>
                    </div>
                </Slider>
            </div>
            <div id = "test" > </div>
            </div>
        );
    }
});
module.exports = Index_Slider;
