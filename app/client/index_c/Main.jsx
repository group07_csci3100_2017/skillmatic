// this module is the main structure of this program. 
var React = require('react');
var Index_Header = require('Index_Header');
var cookie = require('react-cookie');
var Loginout = require('Loginout');
var Footer = require('Footer');
var Content = require('Content');
var About = require('About');
{/*<script src="path/to/js/mfb.js"></script>*/}
var {hashHistory} = require('react-router');
var Course_api = require('Course_api'); 
var Main = React.createClass({
    /**
    * getInitialState() this function will set the initial state by checking its cookie
    * and also the userLoginStatus
    * after that it will set the @loginStatus @username @enrollments @keyword and @events
    */
    getInitialState:function(){
        if(cookie.load('username') === undefined || cookie.load('username') === null){
        return {
            loginStatus: 'notlogin',
            username:"",
            enrollments: [],
            keyword:null,
            events: []
            }
        }else{
            this.resetStatus();
        }
        return {
            loginStatus: 'notlogin',
            username:"",
            enrollments: [],
            keyword:null,
            events: []
            }
    },
    /**
    * resetStatus() this function will reset the cookie and state into (empty or null) or user's current status 
    * if the res == 'notlogin' set all the values in state to empty or null
    * else set the cookie and state as user relatd data
    */
    resetStatus:function(){
        var that = this;
        Loginout.getStatus().then(function(res){
            if(res === 'notlogin'){
             that.setState({
                loginStatus: 'notlogin',
                username:"",
                enrollments: [],
                keyword:null
                });
            }else{
                that.getUserCourse();
                that.getUserEnrollment();
                that.setState({
                loginStatus: 'login',
                username:cookie.load('username'),
                userintro:cookie.load('userintro'),
                useremail:cookie.load('useremail'),
                enrollments: cookie.load('enrollments'),
                keyword:null
                });
            }
        }
        ,function(err){
            console.log(err);
        })
    },
    /**
    * userlogin() we use the userlogin to let user login and upload our state
    * and cookie and after that we also need to recall getUserCourse and getUserEnrollment)
    */
    userlogin:function(status,username,user){
        cookie.save('username', username, { path: '/' });
        cookie.save('userintro', user.selfIntroduction, {path: '/'} );
        cookie.save('useremail',user.email,{path:'/'});
        cookie.save('enrollments', user.enrollments,{path:'/'});
        this.getUserCourse();
        this.getUserEnrollment();
        this.setState(
            {username:username,
            loginStatus:status,
            userintro:user.selfIntroduction,
            useremail:user.email,
            enrollments: user.enrollments})
    },
     /**
    * getUserCourse() we use the getUserCourse to let user get Courses and upload our state
    */
    getUserCourse:function(){
        var that = this;
        Course_api.getUsersCourse().then(function(res){
                that.setState({usercourse:res});
            },function(err){
                console.log(err);
            })
        
    },
    /**
    * userlogout() we use this function to let user logout
    * and delete the corresponding cookie
    * @username the username will be deleted
    * @enrollments the enrollments will be eliminated
    * @useremail the useremial will be deleted
    * @userintro the userintro will be deleted
    */
    userlogout:function(){
        cookie.remove('username');
        cookie.remove('userintro');
        cookie.remove('enrollments');
        cookie.remove('useremail');
        this.setState({
            loginStatus:'notlogin',
            username:'',
            userintro:'',
            useremail:''
        })
    },
    /**
    * search() this function accept the keyword and set the state keyword to be the parameter
    */
    search: function(keyword){
        this.setState({
            keyword: keyword
        })
        hashHistory.push('/course'); 
    },
    /**
    * functiontemp() this will accept the functionA will be used to execute the events value in the state
    */
    functiontemp: function(functionA){
        functionA(this.state.events);
    },
    /**
    * getUserEnrollment() getUserEnrollment which will accept the function and use that to call the getInstructor/getEnrolledSession function in the Course_api module
    */
    getUserEnrollment: function(functionA){
        var that = this;
        if(functionA){
        Course_api.getEnrolledSession().then(function(sessions){
            var i = 0;
            sessions.map(function(session) {
                var sess = session.session;
                Course_api.getCourseById(sess.coursePlan).then(function(course){
                    var events = that.state.events;
                    console.log(events);
                    var end = new Date(sess.classDateTime);
                    end.setMinutes(end.getMinutes() + sess.duration);
                    // console.log(course);
                    Course_api.getInstructor(course.instructor).then(function(res) {
                        course.detailins = res;
                        events[i] = ({
                            title: sess.title,
                            start: new Date(sess.classDateTime),
                            end: end,
                            enrollmentId: session.enrollmentId,
                            course: course
                        });
                        that.setState({events: events})
                        that.functiontemp(functionA);
                        i = i + 1;
                    }, function(err){
                        console.log(err);
                    })
                });
            })
        });}
        else{
         Course_api.getEnrolledSession().then(function(sessions){
            sessions.map(function(session) {
                var sess = session.session;
                Course_api.getCourseById(sess.coursePlan).then(function(course){
                    var events = that.state.events;
                    console.log(events);
                    var end = new Date(sess.classDateTime);
                    end.setMinutes(end.getMinutes() + sess.duration);
                    // console.log(course);
                    Course_api.getInstructor(course.instructor).then(function(res) {
                        course.detailins = res;
                            events.push({
                            title: sess.title,
                            start: new Date(sess.classDateTime),
                            end: end,
                            enrollmentId: session.enrollmentId,
                            course: course
                        });
                        that.setState({events: events})
                    }, function(err){
                        console.log(err);
                    })
                });
            })
        });}   
        
    },
    render:function(){
        return(
            <div >
                <Index_Header userlogin={this.userlogin} userlogout = {this.userlogout} loginStatus = {this.state.loginStatus} username = {this.state.username} userintro = {this.state.userintro} email = {this.state.useremail} usercourse= {this.state.usercourse}
                    enrollments={this.state.enrollments} search = {this.search} events = {this.state.events} refresh = {this.getUserEnrollment}/>
                    {React.cloneElement(this.props.children,
                        {keyword: this.state.keyword, 
                         username: this.state.username, 
                         userlogin:this.userlogin, 
                         loginStatus: this.state.loginStatus, 
                         usercourse: this.state.usercourse, 
                         enrollments: this.state.enrollments, 
                         getUserEnrollment: this.getUserEnrollment})}
                <Footer/>
            </div>
        );
    }
}
);
module.exports = Main;
