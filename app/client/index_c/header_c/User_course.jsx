var React = require('react');
var Background_video = require('Background_video');
var Course_cards = require('Course_cards');
var User_course = React.createClass({
    /**
    * getUserowncourse() this function will return the course_cards for the user
    */
    getUserowncourse:function(){
        
        if(this.props.usercourse&&this.props.loginStatus==='login'){
            return (<div>

                        <Course_cards cardsInfo={this.props.usercourse}/>
                    </div>);
        }
    },
    /**
    * render the whole module to the front-end.
    */
    render:function(){
    if(this.props.loginStatus === 'login' && this.props.r_type === 'nav'){
            return (
                <li className="nav-item" data-toggle="modal" data-target="#Usercourse">
                    <a className="nav-link" >Teacher</a>
                </li>
                );
    }else if (this.props.loginStatus === 'login' && this.props.r_type === 'not'){
            return(
                <div className="modal fade" id="Usercourse" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">The course you create</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <div className="modal-body">
                            {this.getUserowncourse()}
                        </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" className="btn btn-primary">Timetable</button>
                            </div>
                        </div>
                    </div>
                </div>
        );
        }else{
            return (null);
        }
    }
}   
);
module.exports = User_course;
    
