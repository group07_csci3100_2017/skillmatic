var React = require('react');
var Background_video = require('Background_video');
var Course_information = require('Course_information');
var Course_api = require('Course_api');
var Course_cards = require('Course_cards');
var User_course = React.createClass({
    
    getInitialState: function() {
        return {
            usercourse: []
        }
    },

    componentDidMount: function() {
        var that = this;
        Course_api.getUsersCourse().then(function(res){
                console.log(res)
                that.setState({usercourse:res});
            },function(err){
                console.log(err);
            });
    },

    render:function(){
        const jumbotronback = {
            backgroundImage: 'url('+"./images/background-1.png"+')',
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            backgroundSize:'cover',
            textAlign: 'center'
        };
        const text = {
            color: 'white',
            textShadow: '2px 2px 4px #000000'
        }
        if(this.state.usercourse){
            return(
            <div>
                <div className="jumbotron jumbotron-fluid" style = {jumbotronback} ><br/><h1 style = {text}>Courses you create</h1></div>
                <Course_cards refresh = {this.componentDidMount} cardsInfo = {this.state.usercourse} isContainer='true' isIns={true}/>
            </div>
            );
        }else{
            return null;
        }
    }
}   
);
module.exports = User_course;
    
