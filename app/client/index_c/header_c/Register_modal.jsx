var React = require('react');
var Loginout = require('Loginout');
var Register = React.createClass({
    /**
    * getInitialState() will set the username password selfintro and email into null or empty
    */
    getInitialState:function(){
        return {
            username:"",
            password:"",
            selfintro:null,
            email:"",
        }
    },
    /**
    * handleRegister() after the user click the register button this function will sent the request to Loginout module by register function
    * with paramter @username @password @email @selfintro 
    */
    handleRegister: function(e){
        e.preventDefault();
        var that = this; 
        Loginout.register(that.state.username,that.state.password,that.state.email,that.state.selfintro).then(function(res){
            if(res.data.code==='11000'||res.data.message==='A user with the given username is already registered'){
                swal({
                    title: "Warning",
                    text: "Sorry, this username or email has been register",
                    type: "warning",    
                    confirmButtonText: "Close"
                });
            }else{
                swal({
                    title: "Success",
                    text: "Thank you for your registration",
                    type: "success",
                    confirmButtonText: "Close"
                });
            }
            
            
        },function(err){
          console.log(err);  
        })
    },
    /**
    * anytime the user change the state will be updated
    */
    useronChange: function(event){
        this.setState({username: event.target.value});
    },
    /**
    * anytime the password change the state will be updated
    */
    passonChange: function(event){
        this.setState({password: event.target.value});
    },
    /**
    * anytime the email change the state will be updated
    */
    emailChange: function(event){
        this.setState({email: event.target.value});
    },
     /**
    * anytime the selfintro change the state will be updated
    */
    selfintroChange: function(event){
        this.setState({selfintro: event.target.value})
    },
     /**
    * render this module to the frontend
    */
    render:function(){
        if(this.props.loginStatus==='notlogin'&&this.props.r_type==='nav'){
            return(
                <li className="nav-item" data-toggle="modal" data-target="#Register">
                    <a className="nav-link" >Register</a>
                </li>
              );
        }else if(this.props.loginStatus==='notlogin'&&this.props.r_type==='not'){
            return(
            <div className="modal fade" id="Register" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <form onSubmit={this.handleRegister}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Register</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="input-group">
                                <span className="input-group-addon " id="sizing-addon2">❤️</span>
                                <input type="text" className="form-control" placeholder="Username" aria-describedby="sizing-addon2"  onChange={this.useronChange}/><br/>
                            </div>
                                <br/>
                            <div className="input-group">
                                <span className="input-group-addon " id="sizing-addon2">👉</span>
                                <input type="password" className="form-control" placeholder="Password" aria-describedby="sizing-addon2"  onChange={this.passonChange}/><br/>
                            </div>
                                <br/>
                            <div className="input-group">
                                <span className="input-group-addon " id="sizing-addon2">💌</span>
                                <input type="email" className="form-control" placeholder="E-mail" aria-describedby="sizing-addon2"  onChange={this.emailChange}/><br/>
                            </div>
                                <br/>
                            <p>Self Introduction</p>
                            <div className="input-group">
                                <textarea className="form-control" rows="5" id="comment" onChange ={this.selfintroChange}></textarea>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" className="btn btn-primary">Register</button>
                        </div>
                    </div>
                    </form>
                </div>    
            </div>
            )
        }else{
            return(null)
        }
    }});
module.exports = Register;