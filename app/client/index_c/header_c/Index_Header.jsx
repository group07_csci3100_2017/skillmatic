var React = require('react');
var Login_modal = require('Login_modal');
var Register = require('Register');
var {Link} = require('react-router');
var Timetable = require('Timetable');
var User_course_class = require('User_course_class');
var Index_Header = React.createClass({
    /**
    * getInitialState() will set the nevselet as Home
    */
    getInitialState:function(){
        return {
            nevselect:'Home'
        }
    },
    /**
    * renderLogin() this will render the login modal to the front end
    */
    renderLogin:function(){
      if(this.props.loginStatus==='notlogin'){
        return (
        <li className="nav-item" data-toggle="modal" data-target="#myModal">
          <a className="nav-link" >Login</a>                    
        </li>
        );
      }else{
        return(<li className="nav-item" data-toggle="modal" data-target="#myModal"><a className="nav-link">Hi, {this.props.username}</a></li>);
      }
    },
    /**
    * renderTeacher() this will render the Teaher nav link
    */
    renderTeacher: function(){
      if(this.props.loginStatus==='notlogin'){
        return (null);
      }else{
        return(
        <li>
          <Link to="/teacher" className="nav-link" >
          Teacher
          </Link>
        </li>);
      }
    },
    /**
    * searchKeyword() after the search input box got changed we call the searchKeyword to change the state 
    */
    searchKeyword: function(e){
      e.preventDefault();
      this.setState({keywords:e.target.value});
    },
    /**
    * passKeyword() we pass the value of the state to the props function search
    * @search is the function passed by previous module
    */
    passKeyword: function(e){
      e.preventDefault();
      this.props.search(this.state.keywords);
    },
    /**
    * changeColor() set the state of navselect to the parameter selected
    */
    changeColor:function(selected){
      this.setState({navselect:selected})
    },
    /**
    * navClass() set the class of nav after one has been choused
    */
    navClass:function(what){
      if(what === this.state.navselect){
        return ("nav-item active")
      }else{
        return ("nav-item")
      }
    },
    /**
    * render() render the module to the frontend
    */
    render: function(){
        const nav_style = {
            marginBottom:'0px'
        };
        
        return(
          <div>
               <nav className="navbar navbar-toggleable-md navbar-inverse bg-inverse">
               <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
               </button>
               <a className="navbar-brand" href="#">Skillmatic</a>

               <div className="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul className="navbar-nav mr-auto">
                    <li className={this.navClass('Home')}>
                      <Link to="/" className="nav-link"  >
                        Home <span className="sr-only">(current)</span>
                      </Link>
                    </li>
                    <li className={this.navClass('About')} >
                      <Link to="/about" className="nav-link" >
                         About
                      </Link>
                    </li>
                    <li className={this.navClass('Courses')} >
                      <Link to="/course" className="nav-link">
                        Browse
                      </Link>
                    </li>
                      <Register loginStatus = {this.props.loginStatus} r_type = "nav"/>
                      <Timetable loginStatus = {this.props.loginStatus} enrollments={this.props.enrollments} r_type = "nav"/>
                      {this.renderTeacher()}
                      {this.renderLogin()}
                  </ul>
    <form className="form-inline my-4 my-lg-0" onSubmit={this.passKeyword}>
      <input className="form-control mr-sm-3" reftype="text" placeholder="Search" onChange = {this.searchKeyword}/>
      <button className="btn btn-outline-success my-1 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
   <Login_modal loginStatus = {this.props.loginStatus} userlogout = {this.props.userlogout} userlogin={this.props.userlogin} username = {this.props.username} userintro = {this.props.userintro} email = {this.props.email}/>
   <Register loginStatus = {this.props.loginStatus} r_type = "not"/>
   {/*<Timetable loginStatus = {this.props.loginStatus}/>*/}
   <Timetable refresh = {this.props.refresh} loginStatus = {this.props.loginStatus} enrollments={this.props.enrollments} r_type = "not" events = {this.props.events} rerender = {this.props.getUserEnrollment}/>
   {/*<User_course loginStatus = {this.props.loginStatus} usercourse = {this.props.usercourse} r_type = "not"/>*/}
   {/*<Timetable/>*/}
</div>
        );
    
}
});
module.exports = Index_Header;
