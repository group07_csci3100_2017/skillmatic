var React = require('react');
var Login_modal = require('Login_modal');
var Loginout = require('Loginout')
var cookie = require('react-cookie');

var Index_Header = React.createClass({
    /**
    * getDefaultProps() this will set the loginStatus props as notlogin
    */
    getDefaultProps: function(){
      return {
          loginStatus: 'notlogin',

      }  
    },
    /**
    * getInitialState() this will set the loginStatus and username password ... as empty or notlogin
    */
    getInitialState:function(){
        return {
            loginStatus: this.props.loginStatus,
            username:"",
            password:"",
            oldpassword:null,
            newpassword:null,
            newemail:"",
        }
    },
    /**
    * useronChange() this will handle the change from username inputbox by set the state username as the value
    */
    useronChange:function(event){
        this.setState({username: event.target.value});
    },
    /**
    * passonChange() this will handle the change from password inputbox by set the state password as the value
    */
    passonChange:function(event){
        this.setState({password: event.target.value});
    },
    /**
    * selfintroChange() this will handle the change from selfintro inputbox by set the state selfintro as the value
    */
    selfintroChange:function(event){
        this.setState({selfIntro_input: event.target.value});
    },
    /**
    * oldpassonChange() this will handle the change from oldpassword inputbox by set the state oldpassword as the value
    */
    oldpassonChange:function(event){
        this.setState({oldpassword: event.target.value});
    },
    /**
    * newpassonChange() this will handle the change from newpassword inputbox by set the state newpassword as the value
    */
    newpassonChange:function(event){
        this.setState({newpassword: event.target.value});
    },
     /**
    * newemailonChange() this will handle the change from newemail inputbox by set the state newemail as the value
    */
    newemailChange:function(event){
        this.setState({newemail: event.target.value});
    },
     /**
    * handleLogin() after the user click the submit button and sent the login function to server
    */
    handleLogin: function(e) {
        e.preventDefault();
        var that = this;
        Loginout.loginfunction(that.state.username,that.state.password).then(function (res){
            if(res.data.username){
                $('#myModal').modal('hide');
                setTimeout(function(){
                    that.props.userlogin('login',res.data.username,res.data.user)
                },250)
                
            }else{
                swal({
            title: "Invalid!",
            text: "Sorry, the username or password is unvalid. Please try again!",
            type: "error",
            confirmButtonText: "Cool"
            });
            }
        }, function (errorMessage){
            alert(errorMessage);
        })
    },
     /**
    * handleLogout() after the user click the logout button and sent the logout function to server
    */
    handleLogout: function(e) {
        e.preventDefault();
        var that = this;
        Loginout.logout().then(function (res){
            that.props.userlogout();
            swal({
                    title: "Logout Successfully",
                    type: "success",
                    confirmButtonText: "OK~"
                });
        $('#myModal').modal('hide');
        }, function (errorMessage){
            alert(errorMessage);
        })
    },
    /**
    * handleChangepro() after the user change his profile, this will sent the api request to Loginout module
    */
    handleChangepro: function(e){
        e.preventDefault();
        var that = this
        Loginout.changeprof(that.state.selfIntro_input,that.state.newemail,that.state.newpassword,that.state.oldpassword).then(function(res){
            if(that.state.selfIntro_input){
                cookie.save('userintro', that.state.selfIntro_input, {path: '/'} );
                cookie.save('useremail', that.state.newemail, {path:'/'});
            }
            that.refs.newp.value = "";
            that.refs.oldp.value = "";
            if(res === 'account information changed successfully'){
                swal({
                    title: "Successfully change",
                    text: "Ok~ You successfully change your information",
                    type: "success",
                    confirmButtonText: "OK~"
                });
            }else{
                // console.log(res.data.message);
            }
        },function (err){
            alert(err);
        })
    },
    render: function(){
        if(this.props.loginStatus==='notlogin'){
            return(
                <div>
                <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Login</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form onSubmit={this.handleLogin}>
                            <div className="modal-body">
                                <div className="input-group">
                                    <span className="input-group-addon " id="sizing-addon2">❤️</span>
                                    <input type="text" className="form-control" placeholder="Username" aria-describedby="sizing-addon2"  onChange={this.useronChange}/><br/>
                                </div>
                                <br/>
                                <div className="input-group">
                                    <span className="input-group-addon " id="sizing-addon2">👉</span>
                                    <input type="password" className="form-control" placeholder="Password" aria-describedby="sizing-addon2"  onChange={this.passonChange}/><br/>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" className="btn btn-primary" >Login</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }else{
            return(<div>
                <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">{this.props.username}'s profile</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                             <form onSubmit={this.handleChangepro}>
                            <div className="modal-body">
                                    <div className="modal-body">
                                        <p>Hi, {this.props.username} <br/> You can change your information here~</p>
                                        <div className="input-group">
                                            <span className="input-group-addon " id="sizing-addon2">❤️</span>
                                            <input type="text" className="form-control" placeholder={this.props.username} aria-describedby="sizing-addon2"  disabled/><br/>
                                        </div>
                                        <br/>
                                         <p>If you want to change your password, please provide your original password</p>

                                        <div className="input-group">
                                            <span className="input-group-addon " id="sizing-addon2">👉</span>
                                            <input type="password" ref="oldp" className="form-control" placeholder="Old Password" aria-describedby="sizing-addon2"  onChange={this.oldpassonChange}/><br/>
                                        </div><br/>
                                        <div className="input-group">
                                            <span className="input-group-addon " id="sizing-addon2">✨</span>
                                            <input type="password" ref="newp" className="form-control" placeholder="New Password" aria-describedby="sizing-addon2"  onChange={this.newpassonChange}/><br/>
                                        </div><br/>
                                        <div className="input-group">
                                            <span className="input-group-addon " id="sizing-addon2">💌</span>
                                            <input type="text" ref="email" className="form-control" defaultValue={this.props.email}  aria-describedby="sizing-addon2"  onChange={this.newemailChange}/><br/>
                                        </div><br/>
                                        <p>Self Introduction</p>
                                        <div className="input-group">
                                              <textarea className="form-control" rows="5" id="comment" defaultValue={this.props.userintro} onChange ={this.selfintroChange}></textarea>
                                        </div>
                                    </div>
                                    
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" onClick = {this.handleLogout} className="btn btn-danger">Logout</button>
                                <button type="submit" className="btn btn-primary">Save changes</button>
                            </div>
                         </form>

                        </div>
                    </div>
                </div>
            </div>)
        }
    }
});
module.exports = Index_Header;
