var React = require('react');
var ReactDOM = require('react-dom');
var BigCalendar = require('react-big-calendar');
var moment = require('moment');
var Course_api = require('Course_api');
var Course_information = require('Course_information');
import css from 'react-big-calendar/lib/css/react-big-calendar.css';

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

var Timetable_modal = React.createClass({
    getInitialState(){
        return {
            events: this.props.events,
            enrollments: this.props.enrollments
        }
    },

    
    onSelect: function(event) {
        $('#' + event.course._id + 'enroll').modal('show');
        $('#Timetable').modal('hide');
    },
    refreshAll: function(){
        this.props.refresh(this.renderCourseInfo);
        
    },
    renderCourseInfo: function(events) {
        var renderedInfo = [];
        
        for(var i = 0; i < events.length; i++) {
            renderedInfo.push(
                <Course_information refresh = {this.refreshAll} cards={events[i].course} enrollmentId={events[i].enrollmentId}/>
            )
        }
        return renderedInfo;
    },

    onClick: function(event) {
    },

    render: function(){





        if(this.props.loginStatus === 'login' && this.props.r_type === 'nav'){
            return (
                <li className="nav-item" data-toggle="modal" data-target="#Timetable">
                    <a className="nav-link" >Timetable</a>
                </li>
                );
        }else if(this.props.loginStatus === 'login' && this.props.r_type === 'not'){
                return(
                            <div>
                                <div className = "container">
                            <div className="row">
                            {this.renderCourseInfo(this.props.events)}
                            </div>
                            </div>
                            <div className="modal fade" id="Timetable" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog modal-lg" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="exampleModalLabel">Your Timetable</h5>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <div className="modal-body" id="ttt">
                                        <BigCalendar
                                        {...this.props}
                                        events={this.state.events}
                                        step={15}
                                        timeslots={8}
                                        defaultView='month'
                                        defaultDate={new Date(2017, 3, 12)}
                                        onSelectEvent={this.onSelect}
                                        />
                                    </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.onClick}>Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        );
            
        }else{
            return (null);
        }
    }
});
module.exports = Timetable_modal;
