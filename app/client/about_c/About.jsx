//it's a static page tp show the about page
var React = require('react');
var Background_video = require('Background_video');
var About = React.createClass({
  render:function(){
    const bg1 = {
      marginTop: '50px',
      marginRight: '50px',
      marginLeft: '50px'
    };
    return(
      <div>
      <div style={bg1}>
        <div className='row'>
          <div className='col-md-7'>
            <h1 style={{color: '338E7D', paddingTop: '15px', paddingBottom: '15px'}}>Our Vision</h1>
            <p>E-Learning is a modern way to learn professional knowledge and skills through Internet. People can take various courses online to learn skills. Nevertheless, some skills are difficult to learn by just watching videos online. Therefore, we built a sharing platform for people to teach and learn leisure skills by face-to-face instruction.</p>
          </div>
          <div className='col-md-5'>
            <img src='https://static01.nyt.com/images/2011/06/15/travel/15globe-hongkong-workshop/15globe-hongkong-workshop-blog480.jpg' className='img-responsive col-md-12 ' />
          </div>
        </div>
        <div className='jumbotron divparagraph' style={{backgroundImage: "linear-gradient(to bottom, rgba(255,255,255,0.4) 0%,rgba(255,255,255,0.4) 100%), url('http://localiiz.s3.amazonaws.com/business_offers/f5efd110-c82d-012f-4dc3-1231381d55eb/painting_corporate-4.jpg')", backgroundSize: 'cover', backgroundPosition: 'center', width: '100%', marginTop: '20px', marginBottom: '20px'}}>
          <h1 style={{fontFamily: 'Palatino Linotype', color:'white'}}>Skillmatic</h1>
          <h5 style={{fontFamily: 'Book Antiqua', color:'white'}}>is meant to make professionals’ skills more accessible and provide an interactive learning experience.</h5>
        </div>
        <div className='row'>
          <div className='col-md-5'>
            <img src='https://cache-graphicslib.viator.com/graphicslib/thumbs674x446/3675/SITours/dim-sum-cooking-class-in-hong-kong-in-hong-kong-158591.jpg' className='img-responsive col-md-12' />
          </div>
          <div className='col-md-7' style ={{marginBottom: '20pxs'}}>
            <h1 style={{color: '588d37', paddingTop: '15px', paddingBottom: '15px'}}>Our Objectives</h1>
            <ul style={{paddingLeft: '20px'}}>
              <li>Offering a fresh method to share skills</li>
              <li>Building a skills sharing community</li>
              <li>Providing a reliable platform to teach and learn skill</li>
              <li>Helping to organize leisure time activities</li>
            </ul>
          </div>
        </div>
      </div>
         </div>
    );
  }
});
module.exports = About;
    
