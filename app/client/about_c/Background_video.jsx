// this module is used as background
var React = require('react');

var Background_video = React.createClass({
    // render the whole background_video as module
    render:function(){
    return(
        
        <div style={{position: 'fixed', zIndex: 1}}>
            <iframe frameBorder="0" height="100%" width="100%" 
            src="https://youtube.com/embed/M6Dr-0cDkMc?autoplay=1&controls=0&showinfo=0&autohide=1">
            </iframe>
        </div>
        );
    }
}   
);
module.exports = Background_video;
    