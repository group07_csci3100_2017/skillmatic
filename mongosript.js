function makeid(range) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < range; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomCat() {
    var cats = ['Sports', 'DIY', 'Others', 'Cooking', 'Arts', 'Music'];
    var forReturn = [];
    var numOfCat = getRandomInt(1, 6);

    for (var i = 0; i < numOfCat; i++) {
        forReturn.push(cats[i])
    }
    return forReturn;
}

function randomImg() {
    var profileImg = ["/tmp/d610025fda64cce478d09f15645a18a471cb356d.jpg", "/tmp/d610025fda64cce478d09f15645a18a471cb356d.jpg", "/tmp/1cfbfa0b7870a2f4d3e7aafe393520b5d537e544.jpg", "/tmp/1cfbfa0b7870a2f4d3e7aafe393520b5d537e544.jpg"];
    var numOfImg = Math.floor(Math.random() * 3);
    return profileImg[numOfImg];
}
var db1 = connect("localhost:27017/skillmatic");
for (var i = 0; i < 10000; i++) {
    db1.courses.insert({
        profile_image: randomImg(),
        fee: Math.floor(Math.random() * 9000),
        duration: Math.floor(Math.random() * 1000),
        description: makeid(20),
        title: makeid(10),
        categories: randomCat(),
        maxClassSize: Math.floor(Math.random() * 100),
        instructor: ObjectId("58ef3ba8cd8b93507656b958"),
        createAt: ISODate("2017-04-12T15:33:17.202Z"),
        updatedAt: ISODate("2017-04-12T15:33:17.202Z")
    })
}
