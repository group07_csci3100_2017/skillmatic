module.exports = {
    entry: './app/client/app.jsx',
    output: {
        path: __dirname,
        filename: './app/static/bundle.js'
    },
    resolve: {
        root: __dirname,
        alias: {
            Main: 'app/client/index_c/Main.jsx',
            Index_Header: 'app/client/index_c/header_c/Index_Header.jsx',
            Footer: 'app/client/footer_c/Footer.jsx',
            About: 'app/client/about_c/About.jsx',
            Index_Slider: 'app/client/index_c/content_c/Index_Slider.jsx',
            Content: 'app/client/index_c/content_c/Content.jsx',
            Content_menu: 'app/client/index_c/content_c/Content_menu.jsx',
            Content_cards: 'app/client/index_c/content_c/Content_cards.jsx',
            Course_api: 'app/client/api_call/Course_api.jsx',
            Card_youtube: 'app/client/index_c/content_c/youtube_c/Card_youtube.jsx',
            Login_modal: 'app/client/index_c/header_c/Login_modal.jsx',
            Course: 'app/client/course_c/Course.jsx',
            Loginout: 'app/client/api_call/Loginout.jsx',
            Register: 'app/client/index_c/header_c/Register_modal.jsx',
            Course_cards: 'app/client/course_c/Course_cards.jsx',
            Course_create: 'app/client/course_c/Course_create.jsx',
            Background_video: 'app/client/about_c/Background_video.jsx',
            Timetable: 'app/client/index_c/header_c/Timetable_modal.jsx',
            User_course: 'app/client/index_c/header_c/User_course.jsx',
            User_course_class: 'app/client/index_c/header_c/User_course_class.jsx',
            Comments: 'app/client/course_c/Comments.jsx',
            Comment_form: 'app/client/course_c/Comment_form.jsx',
            Course_information: 'app/client/course_c/Course_information.jsx',
            Venue_map: 'app/client/course_c/Venue_map.jsx',
            Session_form: 'app/client/course_c/Session_form.jsx'
        },
        extensions: ['.js', '.jsx', '']
    },
    module: {
        loaders: [{
            loader: 'babel-loader',
            query: {
                presets: ['react', 'es2015']
            },
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components)/
        }, {
            test: /\.css$/, // Only .css files
            loader: 'style!css' // Run both loaders
        }]
    }
};